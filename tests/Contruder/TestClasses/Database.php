<?php

namespace Contruder\TestClasses;

interface Database
{
    function getValue();
}