<?php

namespace Contruder\TestClasses;

class MySqlDatabase implements Database
{
    public function getValue()
    {
        return "Value from MySql Database";
    }

}