<?php

namespace Contruder\TestClasses;

class Controller
{
    private $db;
    private $text;
    
    /**
     * @param Database $database
     * @param string $text
     */
    public function __construct(Database $database, $text)
    {
        $this->db = $database;
        $this->text = $text;
    }
    
    public function handle()
    {
        echo $this->text . " " . $this->db->getValue();
    }
}