<?php

namespace Contruder\TestClasses;

class Node
{
    /**
     * @var Node
     */
    private $next;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @param Node $next
     */
    public function __construct(Node $next = null)
    {
        $this->next = $next;
    }
    
    public function getNext()
    {
       return $this->next; 
    }
    
    /**
     * 
     * @param string $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }
    
    public function getName()
    {
        return $this->name;
    }
}
