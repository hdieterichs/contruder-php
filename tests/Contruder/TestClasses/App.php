<?php

namespace Contruder\TestClasses;

use Contruder\Php\Runtime\Services\EntryPoint;

class App implements EntryPoint
{
    private $controller;
    
    public function __construct(Controller $controller)
    {
        $this->controller = $controller;
    }
    
    public function run()
    {
        $this->controller->handle();
    }
}
