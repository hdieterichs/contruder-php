<?php

namespace Contruder\TestClasses;

class SecuredDatabase implements Database
{

    private $key;
    private $db;
    
    /**
     * 
     * @param Database $database
     * @param string $key
     */
    public function __construct(Database $database, $key = "(protected)")
    {
        $this->db = $database;
        $this->key = $key;
    }

    public function getValue()
    {
        return $this->db->getValue() . " " . $this->key;
    }
}