<?php

namespace Contruder;

use Contruder\Php\TypeSystem\PhpTypeSystem;
use Contruder\ImplementationTree\ImplementationHelper;
use Contruder\Php\Construction\DefaultBuilder;
use Contruder\TestClasses\Node;

class ImplementationTreeTest extends \PHPUnit_Framework_TestCase
{
    public function testNodeChain()
    {
        $typeSystem = PhpTypeSystem::getCurrent();
        $nodeType = $typeSystem->getType('\Contruder\TestClasses\Node');
        $stringType = $typeSystem->getType('string');

        /*
         * $nodeType = Php::typeof(Node::class);
         * $stringType == Php::typeofString();
         */
        
        $nodeImpl = ImplementationHelper::newTypeImplementation($nodeType, array(
            "Name" => new ImplementationTree\DirectImplementation($stringType, "testName"),
            "Next" => ImplementationHelper::newTypeImplementation($nodeType, array(
                "Next" => ImplementationHelper::newTypeImplementation($nodeType, array(
                    "Next" => ImplementationHelper::newTypeImplementation($nodeType, array())
                ))
            ))
        ));
        
        $builder = DefaultBuilder::getBuilder();

        $node = $builder->create($nodeImpl);

        $expectedNode = new Node(new Node(new Node(new Node())));
        $expectedNode->setName("testName");
        
        $this->assertEquals($expectedNode, $node);
    }
}