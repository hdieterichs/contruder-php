<?php

namespace Contruder;

use Contruder\Parser\Tyml\TymlParser;
use Contruder\Php\TypeSystem\PhpTypeMapper;
use Contruder\Php\TypeSystem\PhpTypeSystem;
use Contruder\Php\Construction\DefaultBuilder;
use Contruder\TestClasses\Node;

class TymlParserTest extends \PHPUnit_Framework_TestCase
{
    public function test1()
    {
        $parser = new TymlParser(new PhpTypeMapper(PhpTypeSystem::getCurrent()));
        $nodeImpl = $parser->parse(<<<'TYML'
{!tyml 1.0}
{Node !ns:<<php-namespace:Contruder\TestClasses>>
    Name:<testName>
    Next:{Node
        Next:{Node
            Next:{Node}
        }
    }
}
TYML
        );

        $builder = DefaultBuilder::getBuilder();

        $node = $builder->create($nodeImpl);
        
        $expectedNode = new Node(new Node(new Node(new Node())));
        $expectedNode->setName("testName");
        
        $this->assertEquals($expectedNode, $node);
    }
}
