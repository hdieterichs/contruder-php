<?php

use Contruder\Php\Runtime\Services\ServiceContainer;
use Contruder\ContruderHelper;

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    public function test1()
    {
        /* @var $serviceContainer ServiceContainer */
        ContruderHelper::runServiceContainer(<<<'TYML'
{!tyml 1.0}
{s/ServiceContainer !ns:<php-namespace:Contruder/TestClasses>
                  !ns/r:<php-namespace:Contruder/Php/Runtime>
                  !ns/s:<php-namespace:Contruder/Php/Runtime/Services>
                  !ns/x:<http://www.hediet.de/xsd/contruder/1.0/tags>
                  !ns/t:<http://test.de>
    Initializers: [
       {r/FileServiceContainerLoader FileSelector:
            {r/WildcardFileSelector <ConfigFiles> <*.tyml>} 
                Parser:{r/CurrentParser} Builder:{r/CurrentBuilder}}
    ]
    [
        {s/ImplService <x/EntryPoint>
            {App Controller:{s/Ref <t/Controller>}}
        }

        {s/ImplService <t/Database> TagPriority:null Id:<t/Test2>
            {MySqlDatabase}
        }

        {s/ImplService <t/Controller>
            {Controller Database:{s/Ref <t/Database>} Text:<Test Welt!>}
        }
    ]
}
TYML
, __DIR__ . "/test.tyml");
    }
}