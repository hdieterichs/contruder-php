<?php

namespace Contruder;

use Contruder\Parser\DefaultParser;
use Contruder\Php\Construction\DefaultBuilder;
use Contruder\Php\TypeSystem\PhpTypeMapper;
use Contruder\Php\TypeSystem\PhpTypeSystem;
use PHPUnit_Framework_TestCase;

class GenerateCodeTest extends PHPUnit_Framework_TestCase
{
    
    public function testGenerateCode()
    {
        $parser = DefaultParser::getParser(new PhpTypeMapper(PhpTypeSystem::getCurrent()));
        $nodeImpl = $parser->parseFile(__DIR__ . "/ConfigFiles/ReferencedService.tyml");
        $builder = DefaultBuilder::getBuilder();
        $code = $builder->generatePhpCode($nodeImpl);
        
        echo $code;
    }
    
}