<?php

namespace Contruder;

use Contruder\Php\TypeSystem\Php;
use Contruder\TypeSystem\TypeHelper;

class TypeTest extends \PHPUnit_Framework_TestCase
{
    public function testNodeType()
    {
        $type = Php::typeof("Contruder\\TestClasses\\ComposedType");
        $this->assertEquals("Contruder\\TestClasses\\ComposedType", $type->getName());
        
        
        $ageFeature = TypeHelper::getPropertyFeature($type, "Age");
        $this->assertEquals("Age", $ageFeature->getName());
        $this->assertSame($type, $ageFeature->getOwnerType());
        
        $this->assertSame(Php::typeofInt(), $ageFeature->getRequiredType());
        
        $this->assertSame(Php::typeofDouble(),
                TypeHelper::getPropertyFeature($type, "Factor")->getRequiredType());
        
        $features = array();
        foreach ($type->getSupportedFeatures() as $feature)
            $features[$feature->getName()] = $feature->getRequiredType()->getName();
        
        print_r($features);
    }
}
