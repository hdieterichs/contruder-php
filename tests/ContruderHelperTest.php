<?php

namespace Contruder;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Contruder\TestClasses\Node;
use Contruder\TypeSystem\ContruderType;

class DecodeBase64 implements ValueProvider
{
    /**
     * @var string
     */
    private $data;

    /**
     * @param string $data (default attribute)
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function provideValue(ServiceProvider $serviceProvider)
    {
        return base64_encode($this->data);
    }
}

class ContruderHelperTest extends \PHPUnit_Framework_TestCase
{
    public function test2()
    {
        
        /* @var $s Php\Runtime\Services\ServiceContainer */
        $s = ContruderHelper::loadObjectGraphFromFile(__DIR__ . "/ConfigFiles/ReferencedService.tyml");
        $s->load(function(TestClasses\Database $db) {
            echo $db->getValue();
        }, new Php\Runtime\Services\Tag("http://test.de", "Database"));
    }
    
    public function test1()
    {
        $node = ContruderHelper::loadObjectGraph(<<<'TYML'
{!tyml 1.0}
{Node !ns:<php-namespace:Contruder/TestClasses> !ns/a:<php-namespace:Contruder>
    Name:{a/DecodeBase64 <Test1234>}
}
TYML
);

        $expectedNode = new Node();
        $expectedNode->setName("VGVzdDEyMzQ=");

        $this->assertEquals($expectedNode, $node);
    }
}