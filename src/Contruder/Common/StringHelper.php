<?php

namespace Contruder\Common;

use StringTemplate\Engine;

class StringHelper extends \Nunzion\StringHelper
{
    /**
     * @var \StringTemplate\AbstractEngine
     */
    private static $engine;
    
    public static function format($template, $value)
    {
        if (self::$engine == null)
            self::$engine = new Engine();
        return self::$engine->render($template, $value);
    }
}