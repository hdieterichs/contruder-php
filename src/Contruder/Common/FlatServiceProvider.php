<?php

namespace Contruder\Common;

class FlatServiceProvider extends ServiceProvider
{
    /**
     * @return ServiceProvider
     */
    public static function getEmpty()
    {
        return new FlatServiceProvider(array());
    }
    
    private $services;
    
    public function __construct(array $services)
    {
        foreach ($services as $key => $service)
        {
            if (!is_string($key))
                Expect::that ($services)->_("must use strings as indices, but got {actual}", 
                        array("actual" => $key));
            if ($service == null)
                Expect::that ($services)->_("cannot contain any null values"); 
        }
        
        $this->services = $services;
    }
    
    public function getService($key)
    {
        Expect::that($key)->isString();
        
        if (isset($this->services[$key]))
            return $this->services[$key];
        else
            return null;
    }
}