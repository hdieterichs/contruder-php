<?php

namespace Contruder\Common;

class HierarchicalServiceProvider extends FlatServiceProvider
{
    private $parent;

    public function __construct(ServiceProvider $parent, array $services)
    {
        parent::__construct($services);

        $this->parent = $parent;
    }

    /**
     * @param string $key
     * @return mixed the service.
     */
    public function getService($key)
    {
        $result = parent::getService($key);
        if ($result === null)
            $result = $this->parent->getService($key);

        return $result;
    }
}