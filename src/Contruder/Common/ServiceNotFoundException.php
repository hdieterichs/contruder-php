<?php

namespace Contruder\Common;

class ServiceNotFoundException extends \Exception
{
    public function __construct($key)
    {
        parent::__construct("Service with key $key could not be found.");
    }
}