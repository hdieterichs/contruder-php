<?php

namespace Contruder\Common;

abstract class ServiceProvider
{
    /**
     * @param string $key
     * @return mixed the service.
     */
    public abstract function getService($key);
    
    /**
     * 
     * @param string $key
     * @return mixed
     * @throws ServiceNotFoundException
     */
    public function requireService($key)
    {
        Expect::that($key)->isString();
        
        $service = $this->getService($key);
        if ($service === null)
            throw new ServiceNotFoundException($key);
        return $service;
    }

    /**
     * 
     * @param string $type
     * @return mixed
     * @throws ServiceNotFoundException
     */
    public function requireServiceOfType($type)
    {
        Expect::that($type)->isString();

        $service = $this->getService($type);
        if ($service === null)
            throw new ServiceNotFoundException($type);
        Expect::that($service)->isInstanceOf($type);
        return $service;
    }
}
