<?php

namespace Contruder;

use Contruder\Parser\DefaultParser;
use Contruder\Php\Construction\DefaultBuilder;
use Contruder\Php\Runtime\Services\ServiceContainer;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpType;
use Contruder\Php\TypeSystem\PhpTypeMapper;
use Contruder\Php\TypeSystem\PhpTypeSystem;

class ContruderHelper
{
    /**
     * 
     * @param string $content
     * @param string $filename
     * @param PhpType $expectedType
     * @return mixed
     */
    public static function loadObjectGraph($content, $filename = null, PhpType $expectedType = null)
    {
        $parser = DefaultParser::getParser(new PhpTypeMapper(PhpTypeSystem::getCurrent()));
        
        if ($content === null)
            $nodeImpl = $parser->parseFile($filename);
        else
            $nodeImpl = $parser->parse($content, $filename);
        
        $builder = DefaultBuilder::getBuilder();
        return $builder->create($nodeImpl, $expectedType);
    }
    
    /**
     * @param string $content
     * @param string $filename
     */
    public static function runServiceContainer($content, $filename = null)
    {
        /** @var $serviceContainer ServiceContainer */
        $serviceContainer = self::loadObjectGraph($content, $filename,
            Php::typeof(ServiceContainer::getClassName()));
        $serviceContainer->run();
    }
    
    /**
     * @param string $filename
     */
    public static function loadServiceContainerFromFile($filename, callable $action = null, Tag $tag = null, PhpType $expectedType = null)
    {
        /* @var $serviceContainer ServiceContainer */
        $serviceContainer = self::loadObjectGraph(null, $filename,
            Php::typeof(ServiceContainer::getClassName()));
        return $serviceContainer->load($action, $tag, $expectedType);
    }

    /**
     * @param string $filename
     * @param PhpType $expectedType
     * @return mixed
     */
    public static function loadObjectGraphFromFile($filename, PhpType $expectedType = null)
    {
        return self::loadObjectGraph(null, $filename, $expectedType);
    }
    
    /**
     * @param string $filename
     * @return mixed
     */
    public static function runServiceContainerFromFile($filename)
    {
        self::runServiceContainer(null, $filename);
    }
} 