<?php

namespace Contruder\ImplementationTree;

use Contruder\Common\StringHelper;
use Contruder\TypeSystem\ContruderType;

class DirectImplementation extends Implementation
{
    /**
     * @var ContruderType
     */
    private $type;
    
    /**
     * @var mixed
     */
    private $instance;
    
    public function __construct(ContruderType $type, $instance)       
    {
        $this->type = $type;
        $this->instance = $instance;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function getValue()
    {
        return $this->instance;
    }
    
    public function __toString()
    {
        return StringHelper::format("Instance of type {0}: {1}",
                array($this->type, $this->instance));
    }
}