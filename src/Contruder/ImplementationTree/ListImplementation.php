<?php

namespace Contruder\ImplementationTree;

use Contruder\Common\Expect;
use Contruder\TypeSystem\ContruderType;

class ListImplementation extends Implementation
{
    /**
     * @var ContruderType
     */
    private $type;
    /**
     * @var Implementation[]
     */
    private $items;
    
    public function __construct(ContruderType $type, $items)
    {
        Expect::that($items)->isArrayOf(Implementation::getClassName());
        
        $this->type = $type;
        $this->items = $items;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function getItems()
    {
        return $this->items;
    }
}