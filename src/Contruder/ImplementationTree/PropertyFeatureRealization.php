<?php

namespace Contruder\ImplementationTree;

use Contruder\TypeSystem\PropertyFeature;

final class PropertyFeatureRealization extends FeatureRealization
{
    /**
     * @var PropertyFeature
     */
    private $propertyFeature;
    
    /**
     * @var Implementation
     */
    private $implementation;
    
    public function __construct(PropertyFeature $property, 
            Implementation $implementation)
    {
        $this->propertyFeature = $property;
        $this->implementation = $implementation;
    }
    
    /**
     * @return PropertyFeature
     */
    public function getRealizedFeature()
    {
        return $this->propertyFeature;
    }
    
    /**
     * 
     * @return Implementation
     */
    public function getImplementation()
    {
        return $this->implementation;
    }
}
