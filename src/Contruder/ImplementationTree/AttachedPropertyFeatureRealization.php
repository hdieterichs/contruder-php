<?php

namespace Contruder\ImplementationTree;

use Contruder\TypeSystem\AttachedPropertyFeature;

final class AttachedPropertyFeatureRealization extends FeatureRealization
{
    /**
     * @var AttachedPropertyFeature
     */
    private $attachedPropertyFeature;
    
    /**
     * @var Implementation
     */
    private $implementation;
    
    public function __construct(AttachedPropertyFeature $attachedProperty, 
            Implementation $implementation)
    {
        $this->attachedPropertyFeature = $attachedProperty;
        $this->implementation = $implementation;
    }
    
    /**
     * @return AttachedPropertyFeature
     */
    public function getRealizedFeature()
    {
        return $this->attachedPropertyFeature;
    }
}
