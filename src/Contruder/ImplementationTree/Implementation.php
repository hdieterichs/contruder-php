<?php

namespace Contruder\ImplementationTree;

use Contruder\TypeSystem\ContruderType;

abstract class Implementation
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @return ContruderType
     */
    public abstract function getType();
}