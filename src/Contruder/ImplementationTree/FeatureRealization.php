<?php

namespace Contruder\ImplementationTree;

abstract class FeatureRealization
{
    public static function getClassName()
    {
        return get_called_class();
    }

    public abstract function getRealizedFeature();
}