<?php

namespace Contruder\ImplementationTree;

use Contruder\TypeSystem\ContruderType;
use Contruder\Common\Expect;
use Contruder\TypeSystem\TypeHelper;

class ImplementationHelper
{
    /**
     * @param ContruderType $type
     * @param Implementation[string] $propertyFeatures
     * @return TypeImplementation
     */
    public static function newTypeImplementation(ContruderType $type, $propertyFeatures)
    {
        $realizations = array();
        
        foreach ($propertyFeatures as $key => $implementation)
        {
            if (!is_string($key))
                Expect::that($propertyFeatures)->_("All keys have to be type of string!");
            if (!($implementation instanceof Implementation))
                Expect::that($propertyFeatures)->_("All items have to be type of Implementation!");
            
            $feature = TypeHelper::getPropertyFeatureOrUnknown($type, $key);
            $realizations[] = new PropertyFeatureRealization($feature, $implementation);
        }
        
        return new TypeImplementation($type, $realizations);
    }
    
    
    
    
    public static function printImplementationTreeToArray(Implementation $implementation)
    {
        if ($implementation instanceof DirectImplementation) 
            return self::printDirectImplementationToArray($implementation);
        else if ($implementation instanceof ListImplementation)
            return self::printListImplementationToArray($implementation);
        else if ($implementation instanceof TypeImplementation)
            return self::printTypeImplementationToArray($implementation);
        else
            throw new NotImplementedException();
    }
    
    private static function printDirectImplementationToArray(DirectImplementation $implementation)
    {
        return array(
            "ImplementationType" => "DirectImplementation", 
            "Type" => $implementation->getType()->__toString(), 
            "Value" => $implementation->getValue());
    }
    
    private static function printListImplementationToArray(ListImplementation $implementation)
    {
        $items = array();
        foreach ($implementation->getItems() as $item)
            $items[] = self::printImplementationTreeToArray($item);
        
        
        return array(
            "ImplementationType" => "ListImplementation", 
            "Type" => $implementation->getType()->__toString(), 
            "Items" => $items);
    }
    
    private static function printTypeImplementationToArray(TypeImplementation $implementation)
    {
        $items = array();
        foreach ($implementation->getFeatureRealizations() as $item)
        {
            if ($item instanceof PropertyFeatureRealization)
            {
                $items[$item->getRealizedFeature()->getName()] = self::printImplementationTreeToArray($item->getImplementation());
            }
            else
                throw new NotImplementedException();
        }
        
        return array(
            "ImplementationType" => "TypeImplementation", 
            "Type" => $implementation->getType()->__toString(), 
            "Feature Realizations" => $items);
    }
    
    
}