<?php

namespace Contruder\ImplementationTree;

use Contruder\Common\Expect;
use Contruder\TypeSystem\ContruderType;
use Contruder\TypeSystem\ConstructionSet;

class TypeImplementation extends Implementation
{
    /**
     * @var ContruderType
     */
    private $type;
    
    /**
     * @var FeatureRealization[]
     */
    private $featureRealizations;
    
    public function __construct(ContruderType $type, $featureRealizations)
    {
        Expect::that($featureRealizations)->isArrayOf(FeatureRealization::getClassName());
        
        $this->type = $type;
        $this->featureRealizations = $featureRealizations;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * 
     * @return FeatureRealization[]
     */
    public function getFeatureRealizations()
    {
        return $this->featureRealizations;
    }
    
    /**
     * @return ConstructionSet|null
     */
    public function getBestConstructionSet()
    {
        $currentlyBestConstructionSet = null;
        $matchingFeatures = -1;

        /* @var $constructionSet ConstructionSet */
        foreach ($this->getType()->getConstructionSets() as $constructionSet)
        {
            $requiredFeatures = $constructionSet->getRequiredFeatures();

            $successful = true;
            foreach ($requiredFeatures as $requiredFeature)
            {
                $successful = false;
                foreach ($this->getFeatureRealizations() as $featureRealization)
                {
                    if ($featureRealization->getRealizedFeature() === $requiredFeature)
                    {
                        $successful = true;
                        break;
                    }
                }
                if (!$successful)
                    break;
            }
            
            if ($successful && count($requiredFeatures) > $matchingFeatures)
            {
                $matchingFeatures = count($requiredFeatures);
                $currentlyBestConstructionSet = $constructionSet;
            }
        }

        return $currentlyBestConstructionSet;
    }
}