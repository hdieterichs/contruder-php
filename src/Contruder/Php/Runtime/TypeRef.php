<?php

namespace Contruder\Php\Runtime;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Nunzion\Types\Type;

class TypeRef implements ValueProvider 
{
    public static function getClassName()
    {
        return get_called_class();
    }
    
    private $typeName;

    /**
     * 
     * @param string $typeName
     */
    function __construct($typeName)
    {
        $this->typeName = $typeName;
    }
    
    function provideValue(ServiceProvider $serviceProvider)
    {
        return Type::of($this->typeName);
   }
}
