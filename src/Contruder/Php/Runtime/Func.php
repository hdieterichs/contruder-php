<?php

namespace Contruder\Php\Runtime;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\SolutionFactory;
use Contruder\Php\Construction\ValueProvider;
use Contruder\Php\TypeSystem\Php;

class Func implements ValueProvider 
{
    /**
     * @var SolutionFactory
     */
    private $solutionFactory;

    /**
     * 
     * @param SolutionFactory $solutionFactory (default attribute)
     */
    function __construct(SolutionFactory $solutionFactory)
    {
        $this->solutionFactory = $solutionFactory;
    }
    
    function provideValue(ServiceProvider $serviceProvider)
    {
        $f = $this->solutionFactory;
        return function() use ($f, $serviceProvider) {
            return $f->create(Php::typeofObject(), $serviceProvider);
        };
    }
}

/**
 * @type-param T the result type.
 *
interface CallableInterface
{
    /**
     * @return T the result.
     *
    function __invoke();
}

// Callable<int>
 
 */