<?php

namespace Contruder\Php\Runtime;

use Contruder\Parser\ImplementationParser;
use Contruder\Php\Construction\Builder;
use Contruder\Php\Runtime\Services\Initializer;
use Contruder\Php\Runtime\Services\ServiceContainer;
use Contruder\Php\TypeSystem\Php;
use Nunzion\IO\FileSelector;

class FileServiceContainerLoader implements Initializer
{
    /**
     * @var FileSelector
     */
    private $fileSelector;
    /**
     * @var ImplementationParser
     */
    private $parser;
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(FileSelector $fileSelector,
                                ImplementationParser $parser, Builder $builder)
    {
        $this->fileSelector = $fileSelector;
        $this->parser = $parser;
        $this->builder = $builder;
    }

    /**
     * @return callable
     */
    function initialize()
    {
        $loadedServicesContainers = array();

        foreach ($this->fileSelector->getFiles() as $file)
        {
            $impl = $this->parser->parseFile($file->getPath());

            /** @var $container ServiceContainer */
            $container = $this->builder->create($impl, Php::typeof(ServiceContainer::getClassName()));
            $unloadFunction = $container->load();
            array_push($loadedServicesContainers, $unloadFunction);
        }

        return function() use ($loadedServicesContainers) {
            while (count($loadedServicesContainers) > 0)
            {
                /** @var $unloadFunction callable */
                $unloadFunction = array_pop($loadedServicesContainers);
                $unloadFunction();
            }
        };
    }
}