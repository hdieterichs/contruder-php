<?php

namespace Contruder\Php\Runtime;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Contruder\Parser\ImplementationParser;

class CurrentParser implements ValueProvider
{
    public static function getClassName()
    {
        return get_called_class();
    }
    
    /**
     * @param ServiceProvider $serviceProvider
     * @return ImplementationParser
     */
    function provideValue(ServiceProvider $serviceProvider)
    {
        throw new \Exception("This object should be replaced by the parser!");
    }
}