<?php

namespace Contruder\Php\Runtime;

use Contruder\Parser\DefaultParser;
use Contruder\Php\Construction\DefaultBuilder;
use Contruder\Php\Runtime\Services\Initializer;
use Contruder\Php\Runtime\Services\ServiceContainer;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpTypeMapper;
use Contruder\Php\TypeSystem\PhpTypeSystem;

class DefaultServiceContainerLoader implements Initializer
{
    private $id;
    
    /**
     * @param string $id (default attribute)
     */
    public function __construct($id)
    {
        $this->id = $id;
    }
    
    private function getConfigFiles()
    {
        $result = array();
        
        return $result;
    }

    /**
     * @return callable
     */
    function initialize()
    {
        $loadedServicesContainers = array();

        $parser = DefaultParser::getParser(new PhpTypeMapper(PhpTypeSystem::getCurrent()));
        $builder = DefaultBuilder::getBuilder();

        foreach ($this->getConfigFiles() as $file)
        {
            $impl = $parser->parseFile($file->getPath());

            /** @var $container ServiceContainer */
            $container = $builder->create($impl, Php::typeof(ServiceContainer::getClassName()));
            $unloadFunction = $container->load();
            array_push($loadedServicesContainers, $unloadFunction);
        }

        return function() use ($loadedServicesContainers) {
            while (count($loadedServicesContainers) > 0)
            {
                /** @var $unloadFunction callable */
                $unloadFunction = array_pop($loadedServicesContainers);
                $unloadFunction();
            }
        };
    }
}