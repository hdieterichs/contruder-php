<?php

namespace Contruder\Php\Runtime;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\Builder;
use Contruder\Php\Construction\ValueProvider;

class CurrentBuilder implements ValueProvider
{
    /**
     * @param ServiceProvider $serviceProvider
     * @return Builder
     */
    function provideValue(ServiceProvider $serviceProvider)
    {
        return $serviceProvider->requireServiceOfType(Builder::getClassName());
    }
}