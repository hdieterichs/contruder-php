<?php


namespace Contruder\Php\Runtime\Services;


interface Initializer {
    /**
     * @return callable
     */
    function initialize();
} 