<?php

namespace Contruder\Php\Runtime\Services;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;

class Exporter implements ValueProvider
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var Tag
     */
    private $tag;

    /**
     * @param Tag $tag (default value)
     * @param Id $id (default value)
     */
    public function __construct(Tag $tag, Id $id)
    {
        $this->tag = $tag;
        $this->id = $id;
    }
    
    /**
     * 
     * @param ServiceProvider $serviceProvider
     * @return Func<T>
     */
    public function provideValue(ServiceProvider $serviceProvider)
    {
        return array($this, "exportService");
    }
    
    /**
     * 
     * @param any $serviceInstance
     * @return Action
     */
    public function exportService($serviceInstance)
    {
        //TODO consider Id
        $service = new InstanceService($this->tag, $serviceInstance);
        CurrentServices::add($service);
        
        return function() use ($service)
        {
            CurrentServices::remove($service);
        };
    }
}
