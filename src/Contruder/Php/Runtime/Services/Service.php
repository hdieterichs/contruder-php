<?php

namespace Contruder\Php\Runtime\Services;

abstract class Service {
    public static function getClassName()
    {
        return get_called_class();
    }
    
    
    public abstract function getAnInstance();

    private $tag;
    private $tagPriority;
    private $id;
    private $idPriority;

    /**
     * @param Tag $tag
     * @param int $tagPriority
     * @param Id $id
     * @param int $idPriority
     */
    protected function __construct(Tag $tag, $tagPriority = null, Id $id = null, $idPriority = null)
    {
        if ($tagPriority === null)
            $tagPriority = 1000;
        if ($idPriority === null)
            $idPriority = 1000;
        if ($id === null)
            $id = new Id($tag->getNamespaceUri(), $tag->getName());
        
        $this->tag = $tag;
        $this->tagPriority = $tagPriority;
        $this->id = $id;
        $this->idPriority = $idPriority;
    }

    /**
     * @return Tag
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @return int
     */
    public function getTagPriority()
    {
        return $this->tagPriority;
    }

    /**
     * @return Id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdPriority()
    {
        return $this->idPriority;
    }
}