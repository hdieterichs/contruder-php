<?php


namespace Contruder\Php\Runtime\Services\Internal;


class CachedServicesInstances 
{
    /**
     * @var _CachedServiceValue[]
     */
    private $cachedServices = array();

    /**
     * @return _CachedServiceValue[]
     * @deprecated
     */
    public function internal_getCachedServices()
    {
        return $this->cachedServices;
    }
    
    /**
     * @param mixed $cachedInstance out
     * @param Dependency[] $instanceDependencies out
     * @return bool
     */
    public function tryGetCachedInstance(&$cachedInstance, &$instanceDependencies)
    {
        foreach ($this->cachedServices as $key => $currentCachedService)
        {
            $successful = true;
            $willNeverBeValidAgain = false;

            foreach ($currentCachedService->getDependencies() as $serviceDependency)
            {
                if (!$serviceDependency->getIsStillValid($willNeverBeValidAgain))
                    $successful = false;
                if ($willNeverBeValidAgain)
                    break;
            }

            if ($successful)
            {
                $cachedInstance = $currentCachedService->getValue();
                $instanceDependencies = $currentCachedService->getDependencies();
                return true;
            }

            // Cached instances, whose referenced service will never be valid again, can be removed
            //TODO: They should be disposed!
            if ($willNeverBeValidAgain)
                unset($this->cachedServices[$key]);
        }

        $instanceDependencies = null;
        $cachedInstance = null;
        return false;
    }

    /**
     * @param mixed $cachedInstance
     * @param Dependency[] $instanceDependencies
     */
    public function addCachedInstance($cachedInstance, array $instanceDependencies)
    {
        $this->cachedServices[] = new _CachedServiceValue($cachedInstance, $instanceDependencies);
    }
}

class _CachedServiceValue
{
    private $dependencies;
    private $value;

    public function __construct($value, $dependencies)
    {
        $this->value = $value;
        $this->dependencies = $dependencies;
    }

    /**
     * @return Dependency[]
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}