<?php

namespace Contruder\Php\Runtime\Services\Internal;

use Contruder\Php\Runtime\Services\Service;
use Contruder\Php\Runtime\Services\Tag;

class DependencyToFirstService extends Dependency {

    /**
     * @param ServiceLandscape $serviceTagChain
     * @param Tag $serviceTag
     * @return DependencyToFirstService
     */
    public static function create(ServiceLandscape $serviceTagChain, Tag $serviceTag)
    {
        $actualService = $serviceTagChain->getFirstService($serviceTag);

        return new DependencyToFirstService($serviceTagChain, $serviceTag, $actualService);
    }

    /**
     * @var ServiceLandscape
     */
    private $serviceTagChain;

    /**
     * @var Service
     */
    private $actualService;

    /**
     * @var Tag
     */
    private $serviceTag;

    private function __construct(ServiceLandscape $serviceTagChain, 
            Tag $serviceTag, Service $actualService = null)
    {
        $this->serviceTagChain = $serviceTagChain;
        $this->actualService = $actualService;
        $this->serviceTag = $serviceTag;
    }

    /**
     * @param boolean $willNeverBeValidAgain
     * @return boolean
     */
    public function getIsStillValid(&$willNeverBeValidAgain)
    {
        if ($this->actualService !== null)
        {
            $willNeverBeValidAgain = !$this->serviceTagChain->contains($this->actualService);
            if ($willNeverBeValidAgain)
                return false;
        }
        else
            $willNeverBeValidAgain = false;

        $newService = $this->serviceTagChain->getFirstService($this->serviceTag);
        return $newService === $this->actualService;
    }

    /**
     * @return Service
     */
    public function getReferencedService()
    {
        return $this->actualService;
    }
}