<?php

namespace Contruder\Php\Runtime\Services\Internal;

use Contruder\Common\Expect;
use Contruder\Php\Runtime\Services\Id;
use Contruder\Php\Runtime\Services\Service;
use Contruder\Php\Runtime\Services\Tag;

/**
 * Class ServiceChain
 * Describes a chain between services with the same tag, e.g.:
 * SecureDatabaseProxy -> CacheDatabaseProxy -> MySqlDatabase
 */
class ServiceChain {

    private $tag;
    /**
     * @var ServiceChainLink[Id]
     */
    private $serviceChainLinks;

    /**
     * @var ServiceChainLink
     */
    private $firstChain;

    public function __construct(Tag $tag)
    {
        $this->serviceChainLinks = array();
        $this->tag = $tag;
    }
    
    /**
     * @return ServiceChainLink[]
     * @deprecated
     */
    public function internal_getServiceChainLinks()
    {
        return $this->serviceChainLinks;
    }
    
    public function getTag()
    {
        return $this->tag;
    }

    public function add(Service $service)
    {
        if (!$service->getTag()->equals($this->tag))
            Expect::that($service->getTag())->_("must be equal to '$this->tag''");

        $idHashcode = $service->getId()->getHashCode();
        
        /** @var $serviceChainLink ServiceChainLink */
        if (isset($this->serviceChainLinks[$idHashcode]))
        {
            $serviceChainLink = $this->serviceChainLinks[$idHashcode];
        }
        else
        {
            $serviceChainLink = new ServiceChainLink($this->getTag(), $service->getId(), $service->getTagPriority());
            $this->serviceChainLinks[$idHashcode] = $serviceChainLink;

            if ($this->firstChain === null)
            {
                $this->firstChain = $serviceChainLink;
            }
            else
            {
                $isLast = false;
                $current = $this->firstChain;
                while (!$isLast)
                {
                    if ($serviceChainLink->getTagPriority() >= $current->getTagPriority())
                        break;

                    if ($current->nextServiceChainLink === null)
                        $isLast = true;
                    else
                        $current = $current->nextServiceChainLink;
                }

                if ($isLast)
                {
                    $current->nextServiceChainLink = $serviceChainLink;
                    $serviceChainLink->previousServiceChainLink = $current;
                }
                else
                {
                    if ($current->previousServiceChainLink !== null)
                        $current->previousServiceChainLink->nextServiceChainLink = $serviceChainLink;
                    else
                        $this->firstChain = $serviceChainLink;

                    $serviceChainLink->nextServiceChainLink = $current;
                    $serviceChainLink->previousServiceChainLink = $current->previousServiceChainLink;

                    $current->previousServiceChainLink = $serviceChainLink;
                }
            }
        }

        $serviceChainLink->add($service);
    }

    public function remove(Service $service)
    {
        $idHashcode = $service->getId()->getHashCode();
        if (!isset($this->serviceChainLinks[$idHashcode]))
            Expect::that($service)->_("cannot be removed if it was not added");

        /** @var $serviceChainLink ServiceChainLink */
        $serviceChainLink = $this->serviceChainLinks[$idHashcode];

        $serviceChainLink->remove($service);

        if ($serviceChainLink->getActualService() === null)
        {
            //remove $serviceChainLink from the chain
            if ($serviceChainLink->previousServiceChainLink === null)
                $this->firstChain = $serviceChainLink->nextServiceChainLink;
            else
                $serviceChainLink->previousServiceChainLink->nextServiceChainLink = $serviceChainLink->nextServiceChainLink;

            if ($serviceChainLink->nextServiceChainLink !== null)
                $serviceChainLink->nextServiceChainLink->previousServiceChainLink = $serviceChainLink->previousServiceChainLink;
            unset($this->serviceChainLinks[$idHashcode]);
        }
    }

    /**
     * @return Service
     */
    public function getFirst()
    {
        if ($this->firstChain === null)
            return null;
        return $this->firstChain->getActualService();
    }

    /**
     * @param Id $id
     * @return Service
     */
    public function getNext(Id $id)
    {
        /** @var $serviceChainLink ServiceChainLink */
        $serviceChainLink = $this->serviceChainLinks[$id->getHashCode()];
        $next = $serviceChainLink->nextServiceChainLink;
        if ($next === null)
            return null;

        return $next->getActualService();
    }
} 