<?php

namespace Contruder\Php\Runtime\Services\Internal;

use Contruder\Php\Runtime\Services\Id;
use Contruder\Php\Runtime\Services\Service;
use Contruder\Php\Runtime\Services\Tag;

class DependencyToNextService extends Dependency {

    /**
     * @param ServiceLandscape $serviceTagChain
     * @param Tag $serviceTag
     * @param Id $serviceId
     * @return DependencyToNextService
     */
    public static function create(ServiceLandscape $serviceTagChain, Tag $serviceTag, Id $serviceId)
    {
        $actualService = $serviceTagChain->getNextService($serviceTag, $serviceId);

        return new DependencyToNextService($serviceTagChain, $serviceTag, $serviceId, $actualService);
    }

    /**
     * @var ServiceLandscape
     */
    private $serviceLandscape;
    /**
     * @var Service
     */
    private $actualService;
    /**
     * @var Tag
     */
    private $serviceTag;
    /**
     * @var Id
     */
    private $serviceId;

    private function __construct(ServiceLandscape $serviceLandscape,
                 Tag $serviceTag, Id $serviceId, Service $actualService = null)
    {
        $this->serviceLandscape = $serviceLandscape;
        $this->actualService = $actualService;
        $this->serviceTag = $serviceTag;
        $this->serviceId = $serviceId;
    }

    /**
     * @param boolean $willNeverBeValidAgain
     * @return boolean
     */
    public function getIsStillValid(&$willNeverBeValidAgain)
    {
        if ($this->actualService !== null)
        {
            $willNeverBeValidAgain = !$this->serviceLandscape->contains($this->actualService);
            if ($willNeverBeValidAgain)
                return false;
        }
        else
            $willNeverBeValidAgain = false;

        $newService = $this->serviceLandscape->getNextService($this->serviceTag, $this->serviceId);
        return $newService === $this->actualService;
    }

    /**
     * @return Service
     */
    public function getReferencedService()
    {
        return $this->actualService;
    }
}