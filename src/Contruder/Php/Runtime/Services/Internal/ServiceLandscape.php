<?php


namespace Contruder\Php\Runtime\Services\Internal;

use Contruder\Common\Expect;
use Contruder\Php\Runtime\Services\Id;
use Contruder\Php\Runtime\Services\Service;
use Contruder\Php\Runtime\Services\Tag;
use SplObjectStorage;

class ServiceLandscape
{
    /**
     * @var SplObjectStorage<Service>
     */
    private $addedServices;

    /**
     * @var ServiceChain[Tag]
     */
    private $serviceChains;

    public function __construct()
    {
        $this->addedServices = new SplObjectStorage();
        $this->serviceChains = array();
    }

    /**
     * @return ServiceChain[]
     * @deprecated
     */
    public function internal_getServiceChains()
    {
        return $this->serviceChains;
    }
    
    public function contains(Service $service)
    {
        return $this->addedServices->contains($service);
    }

    public function add(Service $service)
    {
        if ($this->addedServices->contains($service))
            Expect::that($service)->_("cannot be added more than once");

        $tagHashcode = $service->getTag()->getHashCode();
        
        $this->addedServices->attach($service);

        if (isset($this->serviceChains[$tagHashcode]))
            $serviceChain = $this->serviceChains[$tagHashcode];
        else
        {
            $serviceChain = new ServiceChain($service->getTag());
            $this->serviceChains[$tagHashcode] = $serviceChain;
        }

        $serviceChain->add($service);
    }

    public function remove(Service $service)
    {
        if (!$this->addedServices->contains($service))
            Expect::that($service)->_("cannot be removed if it was not added");

        $this->addedServices->offsetUnset($service);
        
        /** @var $serviceChain ServiceChain */
        $serviceChain = $this->serviceChains[$service->getTag()->getHashCode()];
        $serviceChain->remove($service);
        
        if ($serviceChain->getFirst() === null)
        {
            unset($this->serviceChains[$service->getTag()->getHashCode()]);
        }
    }


    /**
     * @param Tag $tag
     * @return Service
     */
    public function getFirstService(Tag $tag)
    {
        $tagHashCode = $tag->getHashCode();
        if (!isset($this->serviceChains[$tagHashCode]))
            return null;

        /** @var $serviceChain ServiceChain */
        $serviceChain = $this->serviceChains[$tagHashCode];

        return $serviceChain->getFirst();
    }

    /**
     * @param Tag $tag
     * @param Id $id
     * @return Service
     */
    public function getNextService(Tag $tag, Id $id)
    {
        $tagHashCode = $tag->getHashCode();
        if (!$this->serviceChains[$tagHashCode])
            return null;

        /** @var $serviceChain ServiceChain */
        $serviceChain = $this->serviceChains[$tagHashCode];

        return $serviceChain->getNext($id);
    }
}