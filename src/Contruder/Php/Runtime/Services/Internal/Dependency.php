<?php


namespace Contruder\Php\Runtime\Services\Internal;


use Contruder\Php\Runtime\Services\Service;

abstract class Dependency {

    /**
     * @param boolean $willNeverBeValidAgain
     * @return boolean
     */
    public abstract function getIsStillValid(&$willNeverBeValidAgain);

    /**
     * @return Service
     */
    public abstract function getReferencedService();
} 