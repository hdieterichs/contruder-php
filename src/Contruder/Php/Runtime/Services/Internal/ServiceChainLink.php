<?php

namespace Contruder\Php\Runtime\Services\Internal;

use Contruder\Php\Runtime\Services\Id;
use Contruder\Php\Runtime\Services\Service;
use Contruder\Php\Runtime\Services\Tag;
use Nunzion\Expect;

class ServiceChainLink {

    /**
     * @var Tag
     */
    private $tag;

    /**
     * @var Id
     */
    private $id;

    /**
     * All services in this array have the same tag and id.
     *
     * @var Service[]
     */
    private $services;

    /**
     * @var Service
     */
    private $actualService;

    /**
     * @var ServiceChainLink
     */
    public $nextServiceChainLink;

    /**
     * @var ServiceChainLink
     */
    public $previousServiceChainLink;

    private $tagPriority;

    public function __construct(Tag $tag, Id $id, $tagPriority)
    {
        $this->tag = $tag;
        $this->id = $id;
        $this->tagPriority = $tagPriority;
    }

    /**
     * @return Tag
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @return Id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTagPriority()
    {
        return $this->tagPriority;
    }

    public function add(Service $service)
    {
        if (!$service->getTag()->equals($this->tag))
            Expect::that($service->getTag())->_("must be equal to '$this->tag''");
        if (!$service->getId()->equals($this->id))
            Expect::that($service->getId())->_("must be equal to '$this->tag''");

        if ($this->actualService !== null)
            throw new \Exception("Not implemented"); //TODO not implemented.
        $this->actualService = $service;
    }

    public function remove(Service $service)
    {
        $this->actualService = null;
    }

    /**
     * @return Service
     */
    public function getActualService()
    {
        return $this->actualService;
    }
}