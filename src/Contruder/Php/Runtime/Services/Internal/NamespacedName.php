<?php

namespace Contruder\Php\Runtime\Services\Internal;

use Contruder\Common\Expect;

class NamespacedName
{
    private $namespaceUri;
    private $name;

    /**
     * @param string $namespaceUri
     * @param string $name
     */
    public function __construct($namespaceUri, $name)
    {
        Expect::that($namespaceUri)->isString();
        Expect::that($name)->isString();

        $this->namespaceUri = $namespaceUri;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNamespaceUri()
    {
        return $this->namespaceUri;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function equals($other)
    {
        if (!($other instanceof NamespacedName))
            return false;
        return $other->getName() === $this->getName() &&
                $other->getNamespaceUri() == $this->getNamespaceUri();
    }

    public function getHashCode()
    {
        return $this->getNamespaceUri() . "#" . $this->getName();
    }
    
    public function __toString()
    {
        return $this->getHashCode();
    }
    
    public function __getOutline()
    {
        return (new \ReflectionClass($this))->getShortName() . ": " . $this->getHashCode();
    }
}