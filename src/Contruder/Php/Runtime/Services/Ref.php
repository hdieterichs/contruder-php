<?php


namespace Contruder\Php\Runtime\Services;


use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;

class Ref implements ValueProvider {

    private $throwExceptionIfNotFound;
    private $tag;

    /**
     * 
     * @param Tag $tag (default attribute)
     * @param bool $throwExceptionIfNotFound
     */
    public function __construct(Tag $tag, $throwExceptionIfNotFound = false)
    {
        $this->tag = $tag;
        $this->throwExceptionIfNotFound = $throwExceptionIfNotFound;
    }

    public function provideValue(ServiceProvider $serviceProvider)
    {
        $result = CurrentServices::getFirst($this->tag);
        if (!$result->getSuccessful() && $this->throwExceptionIfNotFound)
            throw new \RuntimeException("Service '" . $this->tag . "' not found."); //TODO better Exception class
        return $result->getResult();
    }
}