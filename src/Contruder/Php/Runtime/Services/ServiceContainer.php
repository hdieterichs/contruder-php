<?php

namespace Contruder\Php\Runtime\Services;


use Contruder\Common\Expect;
use Contruder\TypeSystem\ContruderType;

class ServiceContainer
{

    public static function getClassName()
    {
        return get_called_class();
    }

    private $services;
    private $initializers;

    /**
     * @param Service[] $services (default attribute)
     * @param Initializer[] $initializers
     */
    public function __construct(array $services, array $initializers = array())
    {
        $this->services = $services;
        $this->initializers = $initializers;
    }

    /**
     * @param callable $action if tag is specified, $action must accept the first service with this tag.
     * @param Tag $tag
     * @param ContruderType $expectedType
     * @return callable|null
     */
    public function load(callable $action = null, Tag $tag = null, ContruderType $expectedType = null)
    {
        $services = $this->services;

        foreach ($services as $service)
        {
            CurrentServices::add($service);
        }

        $initializersStack = array();

        foreach ($this->initializers as $initializer)
        {
            $initializerResult = $initializer->initialize();
            if ($initializerResult !== null)
            {
                Expect::that($initializerResult)->isCallable();
                array_push($initializersStack, $initializerResult);
            }
        }

        $unload = function () use ($services, $initializersStack) {
            while (count($initializersStack) > 0)
            {
                /** @var $uninitializer callable */
                $uninitializer = array_pop($initializersStack);
                $uninitializer();
            }

            foreach ($services as $service)
            {
                CurrentServices::remove($service);
            }
        };

        if ($action === null)
            return $unload;

        if ($tag === null)
            $action();
        else
        {
            $service = CurrentServices::getFirst($tag);
            $action($service->getResult());
        }

        $unload();
        return null;
    }

    public function run()
    {
        $this->load(function(callable $entryPoint) {
            $entryPoint();
        }, new Tag("http://www.hediet.de/xsd/contruder/1.0/tags", "EntryPoint"));
    }
}