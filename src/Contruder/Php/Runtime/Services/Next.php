<?php

namespace Contruder\Php\Runtime\Services;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;

class Next implements ValueProvider 
{
    public static function getClassName()
    {
        return get_called_class();
    }
    
    private $tag;
    private $id;

    /**
     * 
     * @param Tag $tag (default attribute)
     * @param Id $id (default attribute)
     */
    public function __construct(Tag $tag, Id $id)
    {
        $this->tag = $tag;
        $this->id = $id;
    }

    function provideValue(ServiceProvider $serviceProvider)
    {
        $result = CurrentServices::getNext($this->tag, $this->id);
        return $result->getResult();
    }
}