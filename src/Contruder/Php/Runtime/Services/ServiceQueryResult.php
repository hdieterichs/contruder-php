<?php

namespace Contruder\Php\Runtime\Services;

class ServiceQueryResult
{
    private $result;
    private $successful;
    private $id;

    /**
     * Internal method.
     *
     * @return ServiceQueryResult
     */
    public static function createUnsuccessful()
    {
        return new ServiceQueryResult(null, false);
    }

    /**
     * Internal method.
     *
     * @param $result
     * @param Id $id
     * @return ServiceQueryResult
     */
    public static function createSuccessful($result, Id $id)
    {
        return new ServiceQueryResult($result, true, $id);
    }

    /**
     * @param mixed $result
     * @param bool $successful
     * @param Id $id
     */
    private function __construct($result, $successful, Id $id = null)
    {
        $this->result = $result;
        $this->id = $id;
        $this->successful = $successful;
    }

    /**
     * Gets the service instance.
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return boolean
     */
    public function getSuccessful()
    {
        return $this->successful;
    }

    /**
     * The id is null, if the query was not successful.
     *
     * @return Id
     */
    public function getId()
    {
        return $this->id;
    }
}