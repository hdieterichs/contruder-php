<?php

namespace Contruder\Php\Runtime\Services;

use Contruder\Php\Runtime\Services\Internal\CachedServicesInstances;
use Contruder\Php\Runtime\Services\Internal\Dependency;
use Contruder\Php\Runtime\Services\Internal\DependencyToFirstService;
use Contruder\Php\Runtime\Services\Internal\DependencyToNextService;
use Contruder\Php\Runtime\Services\Internal\ServiceLandscape;

/**
 * This class is static.
 */
class CurrentServices
{
    /**
     * @var Dependency[]
     */
    private static $currentDependencies;

    /**
     * @var ServiceLandscape
     */
    private static $serviceLandscape;

    /**
     * This method should be used only for debugging purposes.
     * 
     * @return ServiceLandscape
     * @deprecated
     */
    public static function internal_getServiceLandscape()
    {
        return self::getServiceLandscape();
    }
    
    /**
     * 
     * @return ServiceLandscape
     */
    private static function getServiceLandscape()
    {
        if (self::$serviceLandscape === null)
        {
            self::$serviceLandscape = new ServiceLandscape();
        }
        return self::$serviceLandscape;
    }

    public static function add(Service $service)
    {
        self::getServiceLandscape()->add($service);
    }

    public static function remove(Service $service)
    {
        self::getServiceLandscape()->remove($service);
    }

    /**
     * @param Tag $serviceTag
     * @param Id $serviceId
     * @return ServiceQueryResult
     */
    public static function getNext(Tag $serviceTag, Id $serviceId)
    {
        $serviceDependency = DependencyToNextService::create(
            self::getServiceLandscape(), $serviceTag, $serviceId);
        return self::queryServiceDependency($serviceDependency);
    }

    /**
     * @param Tag $serviceTag
     * @return ServiceQueryResult
     */
    public static function getFirst(Tag $serviceTag)
    {
        $serviceDependency = DependencyToFirstService::create(
            self::getServiceLandscape(), $serviceTag);
        return self::queryServiceDependency($serviceDependency);
    }

    private static function queryServiceDependency(Dependency $serviceDependency)
    {
        if (self::$currentDependencies !== null)
            self::$currentDependencies[] = $serviceDependency;


        $refService = $serviceDependency->getReferencedService();

        if ($refService === null)
            return ServiceQueryResult::createUnsuccessful();

        $result = self::resolveService($refService);

        return ServiceQueryResult::createSuccessful($result, $refService->getId());
    }

    /**
     * 
     * @param Service $service
     * @return CachedServicesInstances
     * @deprecated
     */
    public static function internal_getCachedServicesInstances(Service $service)
    {
        return self::getCachedServicesInstances($service);
    }
    
    /**
     * 
     * @param any $instance
     * @return Service|null
     */
    public static function getServiceOfInstance($instance)
    {
        if (isset($instance->__CurrentServices_service))
            return $instance->__CurrentServices_service;
        return null;
    }
    
    /**
     * 
     * @param Service $service
     * @return CachedServicesInstances
     */
    private static function getCachedServicesInstances(Service $service)
    {
        if (!isset($service->__CurrentServices_instances))
        {
            $serviceInstances = new CachedServicesInstances();
            $service->__CurrentServices_instances = $serviceInstances;
        }
        return $service->__CurrentServices_instances;
    }
        
    private static function resolveService(Service $service)
    {
        $serviceInstances = self::getCachedServicesInstances($service);

        $cachedInstance = null;
        $instanceDependencies = null;
        if (!$serviceInstances->tryGetCachedInstance($cachedInstance, $instanceDependencies))
        {
            $oldDependencies = self::$currentDependencies;
            self::$currentDependencies = array();

            $cachedInstance = $service->getAnInstance();
            $cachedInstance->__CurrentServices_service = $service;
            $instanceDependencies = self::$currentDependencies;
            $serviceInstances->addCachedInstance($cachedInstance, $instanceDependencies);

            self::$currentDependencies = $oldDependencies;
        }

        if (self::$currentDependencies !== null)
            self::$currentDependencies = array_merge(self::$currentDependencies, $instanceDependencies);

        return $cachedInstance;
    }

    private function __construct() {}
}