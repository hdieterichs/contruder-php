<?php

namespace Contruder\Php\Runtime\Services;

use Contruder\Common\FlatServiceProvider;
use Contruder\Php\Construction\SolutionFactory;
use Contruder\Php\TypeSystem\Php;

class DynamicService extends Service
{    
    /**
     * @var SolutionFactory
     */
    private $solutionFactory;

    /**
     * 
     * @param Tag $tag (default attribute)
     * @param SolutionFactory $solutionFactory (default attribute)
     * @param int $tagPriority
     * @param Id $id
     * @param int $idPriority
     */
    public function __construct(Tag $tag, SolutionFactory $solutionFactory, 
            $tagPriority = null, Id $id = null, $idPriority = null)
    {
        parent::__construct($tag, $tagPriority, $id, $idPriority);

        $this->solutionFactory = $solutionFactory;
    }

    public function getAnInstance()
    {
        return $this->solutionFactory->create(Php::typeofObject(),
            FlatServiceProvider::getEmpty());
    }
    
    public function getSolutionFactory()
    {
        return $this->solutionFactory;
    }
}