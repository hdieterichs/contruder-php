<?php

namespace Contruder\Php\Runtime\Services;

class InstanceService extends Service
{
    /**
     * @var mixed
     */
    private $instance;

    /**
     * 
     * @param Tag $tag (default attribute)
     * @param object $instance (default attribute)
     * @param double $tagPriority
     * @param Id $id
     * @param double $idPriority
     */
    public function __construct(Tag $tag, $instance, $tagPriority = null, Id $id = null, $idPriority = null)
    {
        parent::__construct($tag, $tagPriority, $id, $idPriority);

        $this->instance = $instance;
    }

    public function getAnInstance()
    {
        return $this->instance;
    }
}