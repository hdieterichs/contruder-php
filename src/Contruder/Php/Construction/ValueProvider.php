<?php

namespace Contruder\Php\Construction;

use Contruder\Common\ServiceProvider;

interface ValueProvider
{
    /**
     * @param ServiceProvider $serviceProvider
     * @return mixed
     */
    function provideValue(ServiceProvider $serviceProvider);
}
