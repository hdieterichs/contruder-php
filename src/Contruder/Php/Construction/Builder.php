<?php

namespace Contruder\Php\Construction;

use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Common\FlatServiceProvider;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpType;

abstract class Builder
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @param Implementation $implementation
     * @return SolutionFactory
     */
    public abstract function getFactory(Implementation $implementation);
    
    /**
     * @return string
     */
    public abstract function generatePhpCode(Implementation $implementation);
    
    public function create(Implementation $implementation, PhpType $type = null, ServiceProvider $serviceProvider = null)
    {
        if ($type === null)
            $type = Php::typeofObject();
        if ($serviceProvider === null)
            $serviceProvider = FlatServiceProvider::getEmpty();

        $factory = $this->getFactory($implementation);
        return $factory->create($type, $serviceProvider);
    }
}