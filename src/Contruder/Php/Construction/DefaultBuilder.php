<?php

namespace Contruder\Php\Construction;

class DefaultBuilder
{
    private function __construct() {}

    /**
     * @return Builder
     */
    public static function getBuilder()
    {
        return new Builder\DefaultBuilder();
    }
}