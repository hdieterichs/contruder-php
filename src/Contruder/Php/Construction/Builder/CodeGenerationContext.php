<?php

namespace Contruder\Php\Construction\Builder;

abstract class CodeGenerationContext
{
    public static function create($resultVariableName)
    {
        return new _CodeGenerationContextBase($resultVariableName);
    }
    
    /**
     * @var string
     */
    private $resultVariableName;
      
    public function __construct($resultVariableName)
    {
        $this->resultVariableName = $resultVariableName;
    }
        
    public function getResultVariableName()
    {
        return $this->resultVariableName;
    }
    
    public abstract function getFunctionName();
    public abstract function addStatement($statement);
    
    /**
     * 
     * @param string $functionName
     * @param string $resultVariableName
     * @return CodeGenerationContext
     */
    public abstract function createFunctionChild($functionName, $resultVariableName);
    
    /**
     * @param string $variableName
     * @return CodeGenerationContext
     */
    public abstract function createVariableChild($variableName);
    
    public abstract function getCode();
}

abstract class _CodeGenerationFunctionBase extends CodeGenerationContext
{
    /**
     * @var string
     */
    private $baseCode;
    private $usedVariables = array();
    
    public function addStatement($statement)
    {
        $this->baseCode .= $statement . "\n";
    }
    
    public function createVariableChild($resultVariableName)
    {
        if (isset($this->usedVariables[$resultVariableName]))
        {
            $this->usedVariables[$resultVariableName] = $this->usedVariables[$resultVariableName] + 1;
            $variableSuffix = $this->usedVariables[$resultVariableName];
        }
        else
        {
            $this->usedVariables[$resultVariableName] = 0;
            $variableSuffix = "";
        }
                
        return new _CodeGenerationContextVariableChild(
                $resultVariableName . $variableSuffix, $this);
    }
    
    public function getCode()
    {
        return $this->baseCode;
    }
}

class _CodeGenerationContextBase extends _CodeGenerationFunctionBase
{    
    /**
     * @var _CodeGenerationContextFunctionChild[string]
     */
    private $functions = array();
    
    /**
     * @var int
     */
    private $functionCount = 0;
 
    public function __construct($resultVariableName)
    {
        parent::__construct($resultVariableName);
    }
    
    public function getFunctionName()
    {
        return null;
    }
    
    public function createFunctionChild($functionName, $resultVariableName)
    {
        $this->functionCount++;
        
        $context = new _CodeGenerationContextFunctionChild(
                $functionName . $this->functionCount, $resultVariableName, $this);
        
        $this->functions[$context->getFunctionName()] = $context;
        return $context;
    }
    
    public function getCode()
    {
        $result = "";

        foreach ($this->functions as $f)
        {
            $args = '\Contruder\Php\TypeSystem\PhpType $expectedType, \Contruder\Common\ServiceProvider $serviceProvider';
            
            $result .= "function {$f->getFunctionName()}($args) {\n";
            $result .= $f->getCode();
            $result .= "return \${$f->getResultVariableName()};";
            $result .= "}\n";
        }
        
        $result .= parent::getCode();
        
        return $result;
    }
}

class _CodeGenerationContextFunctionChild extends _CodeGenerationFunctionBase
{
    /**
     * @var CodeGenerationContext
     */
    private $parent;

    /**
     * @var string
     */
    private $functionName;
    
    public function __construct($functionName, $resultVariableName, CodeGenerationContext $parent)
    {
        parent::__construct($resultVariableName);
        
        $this->parent = $parent;
        $this->functionName = $functionName;
    }
    
    public function getFunctionName()
    {
        return $this->functionName;
    }
    
    public function createFunctionChild($functionName, $resultVariableName)
    {
        return $this->parent->createFunctionChild($functionName, $resultVariableName);
    }
}

class _CodeGenerationContextVariableChild extends CodeGenerationContext
{
    /**
     * @var CodeGenerationContext
     */
    private $parent;

    public function __construct($resultVariableName, CodeGenerationContext $parent)
    {
        parent::__construct($resultVariableName);
        
        $this->parent = $parent;
    }
    
    public function getFunctionName()
    {
        return $this->parent->getFunctionName();
    }
    
    public function addStatement($statement)
    {
        $this->parent->addStatement($statement);
    }
    
    public function createFunctionChild($functionName, $resultVariableName)
    {
        return $this->parent->createFunctionChild($functionName, $resultVariableName);
    }
    
    public function createVariableChild($variableName)
    {
        return $this->parent->createVariableChild($variableName);
    }
    
    public function getCode()
    {
        return $this->parent->getCode();
    }
}