<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Common\HierarchicalServiceProvider;
use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Php\Construction\Builder;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpType;
use Exception;

class _DefaultImplementationConstructor implements ImplementationConstructor
{
    /**
     * @var ImplementationConstructor
     */
    private $first;
    
    /**
     * @var Builder
     */
    private $builder;
    
    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }
    
    public function setFirst(ImplementationConstructor $value)
    {
        $this->first = $value;
    }
    
    public function create(Implementation $implementation, 
            PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        //Add this builder to the service provider
        //TODO Builder -> CurrentBuilderService
        if ($serviceProvider->getService(Builder::getClassName()) === null)
            $serviceProvider = new HierarchicalServiceProvider($serviceProvider, 
                    array(Builder::getClassName() => $this->builder));

        $result = $this->first->create($implementation, $expectedType, $serviceProvider);

        if (!$expectedType->isTypeOf($result))
        {
            $type = \gettype($result);
            if ($type === "object")
                $type = get_class($result);

            throw new Exception("Types {$expectedType->getName()} and $type do not match!");
        }
        return $result;
    }

    public function generatePhpCode(Implementation $implementation, PhpType $expectedType, CodeGenerationContext $context)
    {
        $this->first->generatePhpCode($implementation, $expectedType, $context);
    }

}

class DefaultBuilder extends Builder
{
    /**
     * @var ImplementationConstructor
     */
    private $builder;
    
    public function __construct()
    {
        $this->builder = new _DefaultImplementationConstructor($this);
        
        $current = new ThrowExceptionImplementationConstructor();
        $current = new ListImplementationConstructor($this->builder, $current);
        $current = new DirectImplementationConstructor($current);
        $current = new TypeImplementationConstructor($this->builder, $current);
        $current = new ValueProviderResolver($current);
        $current = new SolutionFactoryConstructor($this, $this->builder, $current);

        $this->builder->setFirst($current);
    }
    
    public function getFactory(Implementation $implementation)
    {
        return new ConcreteSolutionFactory($this->builder, $implementation);
    }

    public function generatePhpCode(Implementation $implementation)
    {
        $context = CodeGenerationContext::create(null);
        $subContext = $context->createFunctionChild("load", "result");
        
        $context->addStatement('return "' . $subContext->getFunctionName() . '";');
        
        $this->builder->generatePhpCode($implementation, Php::typeofObject(), $subContext);
        
        return $context->getCode();
    }
}