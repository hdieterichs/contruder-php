<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\SolutionFactory;
use Contruder\Php\TypeSystem\PhpType;
use Exception;

class FunctionCallSolutionFactory extends SolutionFactory
{
    private $function;
    
    public function __construct($function)
    {
        $this->function = $function;
    }
    
    public function create(PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        $f = $this->function;
        return $f($expectedType, $serviceProvider);
    }

    public function getImplementation()
    {
        throw new Exception("Not implemented");
    }
}
