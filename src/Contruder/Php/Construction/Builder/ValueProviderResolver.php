<?php

namespace Contruder\Php\Construction\Builder;


use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Php\Construction\ValueProvider;
use Contruder\Php\TypeSystem\PhpType;

class ValueProviderResolver implements ImplementationConstructor
{
    public static function getClassName()
    {
        return get_called_class();
    }
    
    
    public static function resolveValueProvider($instance, ServiceProvider $serviceProvider)
    {
        while ($instance instanceof ValueProvider)
        {
            $instance = $instance->provideValue($serviceProvider);
        }

        return $instance;
    }
    
    /**
     * @var ImplementationConstructor
     */
    private $next;

    public function __construct(ImplementationConstructor $next)
    {
        $this->next = $next;
    }

    function create(Implementation $implementation,
                    PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        $result = $this->next->create($implementation, $expectedType, $serviceProvider);

        return self::resolveValueProvider($result, $serviceProvider);
    }
    
    function generatePhpCode(Implementation $implementation, PhpType $expectedType, 
            CodeGenerationContext $context)
    {
        
        if (!$implementation->getType()->inheritsFrom(\Contruder\Php\TypeSystem\PhpTypeSystem::getCurrent()->getType("Contruder\Php\Construction\ValueProvider")))
        {
            $this->next->generatePhpCode($implementation, $expectedType, $context);
        }
        else 
        {
            $subContext = $context->createVariableChild($context->getResultVariableName() . "Unresolved");

            $this->next->generatePhpCode($implementation, $expectedType, $subContext);

            $sourceVariable = $subContext->getResultVariableName();
            $self = self::getClassName();
            $resultVariableName = $context->getResultVariableName();        
            $context->addStatement("\$$resultVariableName = \\$self::resolveValueProvider(\$$sourceVariable, \$serviceProvider);");
        }
    }
}