<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Php\Construction\SolutionFactory;
use Contruder\Php\TypeSystem\PhpType;
use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;

class ConcreteSolutionFactory extends SolutionFactory
{
    /**
     * @var ImplementationConstructor
     */
    private $implementationConstructor;
    
    /**
     * @var Implementation
     */
    private $implementation;
    
    public function __construct(ImplementationConstructor $implementationConstructor,
            Implementation $implementation)
    {
        $this->implementationConstructor = $implementationConstructor;
        $this->implementation = $implementation;
    }
    
    public function getImplementation()
    {
        return $this->implementation;
    }
    
    public function create(PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        return $this->implementationConstructor->create(
                $this->implementation, $expectedType, $serviceProvider);
    }
}