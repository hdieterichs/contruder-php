<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\ImplementationTree\DirectImplementation;
use Contruder\Php\TypeSystem\PhpType;

class DirectImplementationConstructor implements ImplementationConstructor
{
    /**
     * @var ImplementationConstructor
     */
    private $next;
    
    public function __construct(ImplementationConstructor $next)
    {
        $this->next = $next;
    }
    
    public function create(Implementation $implementation, 
            PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        if (!($implementation instanceof DirectImplementation))
            return $this->next->create($implementation, 
                $expectedType, $serviceProvider);
        return $implementation->getValue();
    }

    function generatePhpCode(Implementation $implementation, PhpType $expectedType, 
            CodeGenerationContext $context)
    {
        if (!($implementation instanceof DirectImplementation))
            return $this->next->generatePhpCode($implementation, $expectedType, $context);

        $instance = $implementation->getValue();
        
        if (is_object($instance) && (get_class($instance) === "Contruder\Parser\_DefaultImplementationParser"))
            $value = "\Contruder\Parser\DefaultParser::getParser()";
        else
            $value = var_export($instance, true);
        
        $resultVariableName = $context->getResultVariableName();
        $context->addStatement("\$$resultVariableName = $value;");
    }
}