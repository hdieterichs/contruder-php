<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\ImplementationTree\Implementation;
use Contruder\Common\ServiceProvider;
use Contruder\Php\TypeSystem\PhpType;

interface ImplementationConstructor
{
    function create(Implementation $implementation, 
            PhpType $expectedType, ServiceProvider $serviceProvider);
    
    function generatePhpCode(Implementation $implementation, PhpType $expectedType, CodeGenerationContext $context);
}