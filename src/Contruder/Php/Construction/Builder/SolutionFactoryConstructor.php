<?php

namespace Contruder\Php\Construction\Builder;


use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Php\Construction\Builder;
use Contruder\Php\Construction\SolutionFactory;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpType;

class SolutionFactoryConstructor implements ImplementationConstructor
{

    /**
     * @var Builder
     */
    private $builder;
    /**
     * @var ImplementationConstructor
     */
    private $next;
    /**
     * @var ImplementationConstructor
     */
    private $first;
    
    public function __construct(Builder $builder, ImplementationConstructor $first, ImplementationConstructor $next)
    {
        $this->builder = $builder;
        $this->next = $next;
        $this->first = $first;
    }

    function create(Implementation $implementation,
                    PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        if ($expectedType !== Php::typeof(SolutionFactory::getClassName()))
            return $this->next->create($implementation, $expectedType, $serviceProvider);

        return $this->builder->getFactory($implementation);
    }
    
    function generatePhpCode(Implementation $implementation, PhpType $expectedType, 
            CodeGenerationContext $context)
    {
        if ($expectedType !== Php::typeof(SolutionFactory::getClassName()))
            return $this->next->generatePhpCode($implementation, $expectedType, $context);

        $functionContext = $context->createFunctionChild("solutionFactory", "result");
        
        $this->first->generatePhpCode($implementation, Php::typeofObject(), $functionContext);
        
        $functionName = $functionContext->getFunctionName();
        $FunctionCallSolutionFactory = FunctionCallSolutionFactory::getClassName();
        $resultVariableName = $context->getResultVariableName();        
        
        $context->addStatement("\$$resultVariableName = new \\$FunctionCallSolutionFactory(\"$functionName\");");
    }
}