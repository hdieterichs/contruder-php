<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\ImplementationTree\ListImplementation;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpType;

class ListImplementationConstructor implements ImplementationConstructor
{
    /**
     * @var ImplementationConstructor
     */
    private $next;
    
    /**
     * @var ImplementationConstructor
     */
    private $builder;
    
    public function __construct(ImplementationConstructor $builder, ImplementationConstructor $next)
    {
        $this->next = $next;
        $this->builder = $builder;
    }
    
    public function create(Implementation $implementation, 
            PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        if (!($implementation instanceof ListImplementation))
            return $this->next->create($implementation, 
                $expectedType, $serviceProvider);
        
        //currently only arrays are supported

        $result = array();

        foreach ($implementation->getItems() as $item)
        {
            //todo: extract type from $expectedTypeName
            $item = $this->builder->create($item, Php::typeofObject(), $serviceProvider);
            $result[] = $item;
        }

        return $result;
    }
    
    function generatePhpCode(Implementation $implementation, PhpType $expectedType, 
            CodeGenerationContext $context)
    {
        if (!($implementation instanceof ListImplementation))
            return $this->next->generatePhpCode($implementation, $expectedType, $context);

        $result = array();

        foreach ($implementation->getItems() as $item)
        {
            //todo: extract type from $expectedTypeName
            $subContext = $context->createVariableChild($context->getResultVariableName() . "Element");
            $item = $this->builder->generatePhpCode($item, Php::typeofObject(), $subContext);
            $result[] = '$' . $subContext->getResultVariableName();
        }
        
        $resultVar = $context->getResultVariableName();
        $elementsStr = implode(", ", $result);
        
        $context->addStatement("\$$resultVar = array($elementsStr);");
    }
}