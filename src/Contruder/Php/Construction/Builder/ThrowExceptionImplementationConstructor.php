<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Php\TypeSystem\PhpType;

class ThrowExceptionImplementationConstructor implements ImplementationConstructor
{
    public function create(Implementation $implementation, 
            PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        //no appropriate implementation constructor was found
        throw new \LogicException("Implementation cannot be created!");
    }
    
    function generatePhpCode(Implementation $implementation, PhpType $expectedType, 
            CodeGenerationContext $context)
    {
        //no appropriate implementation constructor was found
        throw new \LogicException("Implementation cannot be created!");
    }
        
}