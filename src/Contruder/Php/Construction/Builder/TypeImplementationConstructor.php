<?php

namespace Contruder\Php\Construction\Builder;

use Contruder\Common\ServiceProvider;
use Contruder\Common\StringHelper;
use Contruder\ImplementationTree\Implementation;
use Contruder\ImplementationTree\PropertyFeatureRealization;
use Contruder\ImplementationTree\TypeImplementation;
use Contruder\Php\TypeSystem\PhpConstructionSet;
use Contruder\Php\TypeSystem\PhpType;
use Exception;
use LogicException;
use Nunzion\NotImplementedException;

class TypeImplementationConstructor implements ImplementationConstructor
{
    /**
     * @var ImplementationConstructor
     */
    private $next;
    
    /**
     * @var ImplementationConstructor
     */
    private $builder;
    
    public function __construct(ImplementationConstructor $builder, ImplementationConstructor $next)
    {
        $this->next = $next;
        $this->builder = $builder;
    }
    
    public function create(Implementation $implementation, 
            PhpType $expectedType, ServiceProvider $serviceProvider)
    {
        if (!($implementation instanceof TypeImplementation))
            return $this->next->create($implementation, 
                $expectedType, $serviceProvider);
 
        $constructionSet = $implementation->getBestConstructionSet();

        //TODO better exception
        if ($constructionSet === null)
            throw new Exception("No construction set available for {$implementation->getType()->getName()}!");

        if (!($constructionSet instanceof PhpConstructionSet))
            throw new Exception("Invalid construction set!");

        $featureRealizations = $implementation->getFeatureRealizations();

        $values = array();
        $usedFeatureRealizations = array();

        foreach ($constructionSet->getRequiredFeatures() as $feature)
        {
            $realization = null;
            foreach ($featureRealizations as $featureRealization)
            {
                if ($featureRealization->getRealizedFeature() === $feature)
                {
                    $realization = $featureRealization;
                    break;
                }
            }

            if ($realization == null)
                throw new LogicException("No realization of feature " . $feature->getName() . " found!");
            if (!($realization instanceof PropertyFeatureRealization))
                throw new LogicException("Realization has to be a property feature realization!");
            
            $usedFeatureRealizations[] = $realization;

            $type = $realization->getRealizedFeature()->getRequiredType();
            if (!($type instanceof PhpType))
                throw new Exception("Not supported type");

            $values[] = $this->builder->create($realization->getImplementation(), $type, $serviceProvider);
        }
        
        $result = $constructionSet->construct($values);
        
        //look for setters
        //TODO: $featureRealization->getSetterMethodName() which can return null
        foreach ($featureRealizations as $featureRealization)
        {
            if (in_array($featureRealization, $usedFeatureRealizations))
                continue;
            
            if (!($featureRealization instanceof PropertyFeatureRealization))
                throw new NotImplementedException();
            
            $methodName = "set" . $featureRealization->getRealizedFeature()->getName();
            
            if (method_exists($result, $methodName))
            {
                $type = $featureRealization->getRealizedFeature()->getRequiredType();
                if (!($type instanceof PhpType))
                    throw new Exception("Not supported type");

                $value = $this->builder->create($featureRealization->getImplementation(), $type, $serviceProvider);
                
                $result->$methodName($value);
            }
        }
        
        return $result;
    }
    
    function generatePhpCode(Implementation $implementation, PhpType $expectedType, 
            CodeGenerationContext $context)
    {
        if (!($implementation instanceof TypeImplementation))
            return $this->next->generatePhpCode($implementation, $expectedType, $context);

        $constructionSet = $implementation->getBestConstructionSet();

        //TODO better exception
        if ($constructionSet === null)
            throw new Exception("No construction set available for {$implementation->getType()->getName()}!");

        if (!($constructionSet instanceof PhpConstructionSet))
            throw new Exception("Invalid construction set!");

        $featureRealizations = $implementation->getFeatureRealizations();

        $values = array();
        $usedFeatureRealizations = array();

        foreach ($constructionSet->getRequiredFeatures() as $feature)
        {
            $realization = null;
            foreach ($featureRealizations as $featureRealization)
            {
                if ($featureRealization->getRealizedFeature() === $feature)
                {
                    $realization = $featureRealization;
                    break;
                }
            }

            if ($realization == null)
                throw new LogicException("No realization of feature " . $feature->getName() . " found!");
            if (!($realization instanceof PropertyFeatureRealization))
                throw new LogicException("Realization has to be a property feature realization!");
            
            $usedFeatureRealizations[] = $realization;

            $type = $realization->getRealizedFeature()->getRequiredType();
            if (!($type instanceof PhpType))
                throw new Exception("Not supported type");

            $variableContext = $context->createVariableChild(StringHelper::uncapitalize($feature->getName()));
            $this->builder->generatePhpCode($realization->getImplementation(), $type, $variableContext);
            $values[] = '$' . $variableContext->getResultVariableName();
        }
        
        /* @var $type PhpType */
        $type = $implementation->getType();
        $resultVar = $context->getResultVariableName();
        $typeName = $type->getName();
        
        $args = implode(", ", $values);
        $context->addStatement("\$$resultVar = new \\$typeName($args);");
        
        $reflector = $type->getReflectionClass();
        
        //look for setters
        //TODO: $featureRealization->getSetterMethodName() which can return null
        foreach ($featureRealizations as $featureRealization)
        {
            if (in_array($featureRealization, $usedFeatureRealizations))
                continue;
            
            if (!($featureRealization instanceof PropertyFeatureRealization))
                throw new NotImplementedException();
            
            $featureName = $featureRealization->getRealizedFeature()->getName();
            $methodName = "set" . $featureName;
            
            if ($reflector->hasMethod($methodName))
            {
                $type = $featureRealization->getRealizedFeature()->getRequiredType();
                if (!($type instanceof PhpType))
                    throw new Exception("Not supported type");

                $valueContext = $context->createVariableChild(StringHelper::uncapitalize($featureName));
                $this->builder->generatePhpCode($featureRealization->getImplementation(), $type, $valueContext);
                $context->addStatement("\${$resultVar}->$methodName(\${$valueContext->getResultVariableName()});");
            }
        }
    }
}