<?php

namespace Contruder\Php\Construction;

use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Php\TypeSystem\PhpType;

abstract class SolutionFactory
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @return Implementation
     */
    public abstract function getImplementation();
    
    public abstract function create(PhpType $expectedType, ServiceProvider $serviceProvider);
}
