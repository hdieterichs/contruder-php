<?php

namespace Contruder\Php\TypeSystem;

use \Contruder\TypeSystem\ConstructionSet;
use \Contruder\TypeSystem\Feature;
use \Contruder\Common\Expect;
use ReflectionClass;

class PhpConstructionSet extends ConstructionSet
{
    /**
     * @var Feature[]
     */
    private $requiredFeatures;
    
    /**
     * @var ReflectionClass
     */
    private $reflectionClass;
    
    public function __construct($requiredFeatures, ReflectionClass $reflectionClass)
    {
        Expect::that($requiredFeatures)->isArrayOf(Feature::getClassName());
        
        $this->requiredFeatures = $requiredFeatures;
        $this->reflectionClass = $reflectionClass;
    }
    
    public function getRequiredFeatures()
    {
        return $this->requiredFeatures;
    }
    
    public function construct($values)
    {
        return $this->reflectionClass->newInstanceArgs($values);
    }
}