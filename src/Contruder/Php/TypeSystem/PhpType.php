<?php

namespace Contruder\Php\TypeSystem;

use Contruder\TypeSystem\ContruderType;
use Contruder\Common\Expect;
use Contruder\Common\StringHelper;
use Contruder\Php\TypeSystem\Reflector\ReflectionClass;
use Contruder\Php\TypeSystem\Reflector\ReflectionParameter;

class PhpType extends ContruderType
{
    const BoolName = "bool";
    const BooleanName = "boolean";
    const IntName = "int";
    const IntegerName = "integer";
    const ArrayName = "array";
    const ObjectName = "object";
    const StringName = "string";
    const DoubleName = "double";
    const RealName = "real";
    const FloatName = "float";
    const CallableName = "callable";
    
    public static function internalCreate($fullTypeName, PhpTypeSystem $typeSystem)
    {
        $genericTypeArguments = array();
        if (StringHelper::endsWith($fullTypeName, "[]"))
        {
            $typeName = StringHelper::removeEnd($fullTypeName, "[]");
            $genericTypeArguments[] = $typeSystem->getType($typeName);
            $fullTypeName = "array";
        }
        
        return new PhpType($fullTypeName, $typeSystem, $genericTypeArguments);
    }
    
    const BaseType = 0;
    const ClassType = 1;
    const InterfaceType = 2;
    
    
    /**
     * @var string
     */
    private $typeName;
    
    private $type;
    private $typeTest;

    /**
     * @var ReflectionClass
     */
    private $reflectionClass;
    
    private $initialized = false;
    
    private $supportedFeatures;
    private $defaultAttachedProperty;
    private $defaultProperties;
    private $constructionSets;
    private $predefinedValues;
    
    /**
     * @var PhpType[]
     */
    private $genericTypeArguments;
    
    /**
     * @var PhpTypeSystem
     */
    private $typeSystem;
    
    private function __construct($fullTypeName, PhpTypeSystem $typeSystem, 
            array $genericTypeArguments)
    {
        //interface, class, base type
        //MyClass<int,bool>
        //string[] => array<string>
        //string[string] => array<string, string>
        
        if (strtolower($fullTypeName) === self::RealName || strtolower($fullTypeName) == self::FloatName)
            $fullTypeName = self::DoubleName;
        
        if (strtolower($fullTypeName) === self::IntegerName)
            $fullTypeName = self::IntName;
        
        if (strtolower($fullTypeName) === self::BooleanName)
            $fullTypeName = self::BoolName;
        
        $baseTypes = array(
            self::BoolName => "is_bool",
            self::IntName => "is_int",
            self::ArrayName => "is_array",
            self::ObjectName => function ($other) { return true; },
            self::StringName => "is_string",
            self::DoubleName => "is_double",
            self::CallableName => "is_callable");

        if (array_key_exists(strtolower($fullTypeName), $baseTypes))
        {
            $this->type = self::BaseType;
            $this->typeTest = $baseTypes[strtolower($fullTypeName)];
        }
        else if (class_exists($fullTypeName))
        {
            $this->type = self::ClassType;
            $this->typeTest = function($t) use ($fullTypeName) {
                return $t instanceof $fullTypeName;
            };
        }
        else if (interface_exists($fullTypeName))
        {
            $this->type = self::InterfaceType;
            $this->typeTest = function($t) use ($fullTypeName) {
                return $t instanceof $fullTypeName;
            };
        }
        else
            Expect::that($fullTypeName)->_(
                    "must be a valid type name, but '{actual}' is not a valid type name", 
                    array("actual" => $fullTypeName));

        $this->typeName = $fullTypeName;
        $this->typeSystem = $typeSystem;
        $this->genericTypeArguments = $genericTypeArguments;
    }

    public function isTypeOf($object)
    {
        if ($object === null)
            return true;
        $f = $this->typeTest;
        return $f($object);
    }
    
    public function getName()
    {
        $genericArgs = "";
        if (count($this->genericTypeArguments) > 0)
        {
            foreach ($this->genericTypeArguments as $arg)
            {
                if ($genericArgs === "")
                    $genericArgs = "<";
                else
                    $genericArgs .= ", ";
                $genericArgs .= $arg->getName();
            }
            $genericArgs .= ">";
        }
        return $this->typeName . $genericArgs;
    }       
    
    private function initializeClass()
    {
        $this->supportedFeatures = array();
        $this->constructionSets = array();

        $this->reflectionClass = new ReflectionClass($this->typeName, $this->typeSystem);

        $this->defaultProperties = array();
        
        $constructor = $this->reflectionClass->getConstructor();

        if ($constructor !== null && $constructor->isPublic())
        {
            $constructorRequiredFeatures = array();
            
            /* @var $param ReflectionParameter */
            foreach ($constructor->getParameters() as $param)
            {
                $featureName = StringHelper::capitalize($param->getName());

                $requiredType = $param->getRequiredType();

                $feature = new PhpPropertyFeature($featureName, $this, $requiredType);
                $this->supportedFeatures[] = $feature;
                $constructorRequiredFeatures[] = $feature;
                
                if (StringHelper::endsWith(trim($param->getDescription()), 
                        "(default attribute)"))
                {
                    $this->defaultProperties[] = $feature;
                }
            }

            $optionalParameters = $constructor->getNumberOfParameters()
                    - $constructor->getNumberOfRequiredParameters();
            
            for ($i = 0; $i <= $optionalParameters; $i++)
            {
                $requiredFeatures = array_slice($constructorRequiredFeatures,
                        0, $constructor->getNumberOfRequiredParameters() + $i);
                $this->constructionSets[] = new PhpConstructionSet(
                        $requiredFeatures, $this->reflectionClass);
            }
        }
        else if ($constructor === null)
        {
            $this->constructionSets[] = new PhpConstructionSet(array(), $this->reflectionClass);
        }

        //get all set methods
        //TODO don't add a feature twice

        /* @var $method \ReflectionMethod */
        foreach ($this->reflectionClass
                ->getMethods(\ReflectionMethod::IS_PUBLIC) as $method)
        {
            if (StringHelper::startsWith($method->getName(), "set") &&
                    $method->getNumberOfParameters() === 1)
            {
                $featureName = StringHelper::capitalize(
                        substr($method->getName(), strlen("set")));
                if ($featureName !== "" || $featureName !== null)
                {
                    $params = $method->getParameters();
                    $requiredType = $params[0]->getRequiredType();

                    $this->supportedFeatures[] = 
                            new PhpPropertyFeature($featureName, $this, $requiredType);
                }
            }
        }

        //TODO enable overloading with static create1, create2 methods

        //TODO and default features
    }
    
    
    private function initialize()
    {
        if ($this->type === self::ClassType)
        {
            $this->initializeClass();
        }
        else if ($this->type === self::InterfaceType)
        {
            $this->supportedFeatures = array();
            $this->constructionSets = array();
            $this->predefinedValues = array();
            $this->defaultProperties = array();
        }
        else if ($this->type === self::BaseType)
        {
            $this->supportedFeatures = array();
            $this->constructionSets = array();
            $this->predefinedValues = array();
            $this->defaultProperties = array();
            
            if ($this->typeName === self::BoolName)
                $this->predefinedValues = array("true" => true, "false" => false);
        }

        $this->initialized = true;
    }
    
    
        
    public function getPredefinedValues()
    {
        if (!$this->initialized)
            $this->initialize();
        
        return $this->predefinedValues;
    }
    
    public function getSupportedFeatures()
    {
        if (!$this->initialized)
            $this->initialize();
        
        return $this->supportedFeatures;
    }
    
    public function getDefaultAttachedProperty()
    {
        if (!$this->initialized)
            $this->initialize();
        
        return $this->defaultAttachedProperty;
    }
    
    public function getDefaultProperties()
    {
        if (!$this->initialized)
            $this->initialize();
        
        return $this->defaultProperties;
        
    }
    
    public function getConstructionSets()
    {
        if (!$this->initialized)
            $this->initialize();
                
        return $this->constructionSets;
    }
    
    public function getGenericTypeArguments()
    {
        return $this->genericTypeArguments;
    }
    
    public function isGenericTypeParameter()
    {
        
    }
    
    public function getGenericTypeParameterOwner()
    {
        
    }
    
    public function isGenericType()
    {
        
    }
     
    public function getUnderlyingGenericType()
    {
        
    }
    
    public function acceptsList()
    {
        return $this->typeName === "array";
    }
   
    public function getDerivedTypes()
    {
        
    }
    
    public function inheritsFrom(ContruderType $type)
    {
        if (!($type instanceof PhpType))
            return;
        
        if ($this->type === self::ClassType)
        {
            $interfaces = class_implements($this->getName());

            return isset($interfaces[$type->getName()]);
        }
        return false;
    }
    
    public function __toString()
    {
        return $this->getName();
    }
    
    public function getReflectionClass()
    {
        return $this->reflectionClass;
    }
}