<?php

namespace Contruder\Php\TypeSystem;

use Contruder\TypeSystem\ContruderType;
use Contruder\Common\StringHelper;

class PhpTypeSystem
{
    /**
     * @var PhpTypeSystem
     */
    private static $current;
    
    /**
     * @return PhpTypeSystem
     */
    public static function getCurrent()
    {
        if (self::$current === null)
            self::$current = new PhpTypeSystem();
        return self::$current;
    }
    
    /**
     *
     * @var PhpType[string]
     */
    private $types;
    
    /**
     * 
     * @param string $fullTypeName
     * @return ContruderType
     */
    public function getType($fullTypeName)
    {
        $fullTypeName = StringHelper::removeStart($fullTypeName, "\\");
        
        if (!isset($this->types[$fullTypeName]))
            $this->types[$fullTypeName] = PhpType::internalCreate($fullTypeName, $this);
        
        return $this->types[$fullTypeName];
    }
}