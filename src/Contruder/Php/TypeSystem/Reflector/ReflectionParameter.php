<?php

namespace Contruder\Php\TypeSystem\Reflector;

use Contruder\Php\TypeSystem\PhpType;
use Contruder\Php\TypeSystem\PhpTypeSystem;
use Contruder\Common\StringHelper;
use Doctrine\Common\Reflection\StaticReflectionParser;
use Doctrine\Common\Reflection\ClassFinderInterface;

class ReflectionParameter extends \ReflectionParameter
{
    /**
     * @var PhpTypeSystem
     */
    private $typeSystem;

    private $requiredType;
    
    private $description;
    
    private $initialized = false;
    
    public function __construct($function, $parameter, PhpTypeSystem $typeSystem)
    {
        parent::__construct($function, $parameter);
        
        $this->typeSystem = $typeSystem;
    }
    
    private function initialize()
    {
        if ($this->initialized)
            return;

        $processedDocComment = $this->processPHPDoc($this->getDeclaringFunction());

        $this->requiredType = $this->getParamType($processedDocComment);

        if (isset($processedDocComment["params"]["$" . $this->getName()]))
            $this->description = 
                $processedDocComment["params"]["$" . $this->getName()]["description"];
        
        $this->initialized = true;
    }
    
    /**
     * Uses either doc comment or hinted type to get the required type.
     * @return PhpType
     */
    public function getRequiredType()
    {
        $this->initialize();
            
        return $this->requiredType;
    }
    
    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        $this->initialize();
        
        return $this->description;
    }


    //TODO write a reasonable phpdoc processor!
    //taken from http://gonzalo123.com/2011/04/04/reflection-over-phpdoc-with-php/
    private function processPHPDoc(\ReflectionMethod $reflect)
    {
        $phpDoc = array('params' => array());
        $docComment = $reflect->getDocComment();

        if (trim($docComment) == '') {
            return $phpDoc;
        }
        $docComment = preg_replace('#[ \t]*(?:\/\*\*|\*\/|\*)?[ ]{0,1}(.*)?#', '$1', $docComment);
        $docComment = ltrim($docComment, "\r\n");
        $parsedDocComment = $docComment;
        $lineNumber = $firstBlandLineEncountered = 0;
        while (($newlinePos = strpos($parsedDocComment, "\n")) !== false) {
            $lineNumber++;
            $line = substr($parsedDocComment, 0, $newlinePos);
            if ($lineNumber > 100) break; //TODO
            $matches = array();
            if ((strpos($line, '@') === 0) && (preg_match('#^(@\w+.*?)(\n)(?:@|\r?\n|$)#s', $parsedDocComment, $matches))) {
                $tagDocBlockLine = $matches[1];
                $matches2 = array();

                if (!preg_match('#^@(\w+)(\s|$)#', $tagDocBlockLine, $matches2)) {
                    break;
                }
                $matches3 = array();
                if (!preg_match('#^@(\w+)\s+([\w|\\\\\[\\]]+)(?:\s+(\$\S+))?(?:\s+(.*))?#s', $tagDocBlockLine, $matches3)) {
                    break;
                }
                if ($matches3[1] != 'param') {
                    if (strtolower($matches3[1]) == 'return') {
                        $phpDoc['return'] = array('type' => $matches3[2]);
                    }
                } else {
                    $description = "";
                    for ($i = 3; $i < count($matches3); $i++)
                        $description .= " " . $matches3[$i];
                    
                    $phpDoc['params'][$matches3[3]] = array('name' => $matches3[3], 
                        'type' => $matches3[2], 'description' => trim($description));
                }

                $parsedDocComment = str_replace($matches[1] . $matches[2], '', $parsedDocComment);
            }
        }
        return $phpDoc;
    }
    
    private function getParamType(array $processedPhpDoc)
    {
        $paramTypeName = $this->getParamTypeName($processedPhpDoc);
        
        if ($paramTypeName === null)
            $paramTypeName = "object";
        
        return $this->typeSystem->getType($paramTypeName);
    }  
    
    private function getParamTypeName(array $processedPhpDoc)
    {
        $varName = '$' . $this->getName();
        
        $class = $this->getClass();
        $hintedParamType = ($class != null) ? StringHelper::removeStart($class->name, "\\") : null;
        $phpDocParamType = isset($processedPhpDoc["params"][$varName]) ?
                StringHelper::removeStart($processedPhpDoc["params"][$varName]["type"], "\\") : null;
        
        if ($phpDocParamType !== null)
        {
            $keywords = array("string", "int", "integer", "bool", "boolean", "float", "array", "callable", "mixed", "double", "float", "real", "object");

            if (!in_array(strtolower(StringHelper::removeEnd($phpDocParamType, "[]")), $keywords) 
                    && !StringHelper::startsWith($phpDocParamType, "\\"))
            {
                //convert relative name to absolute
                
                $p = new StaticReflectionParser($this->getDeclaringClass()->getName(), new ReflectionFinder());

                $m = $p->getReflectionMethod($this->getDeclaringFunction()->getName());

                $useStatements = $m->getUseStatements();
                
                $matches = array();
                preg_match("/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/", $phpDocParamType, $matches);
                $firstName = $matches[0];
                
                if (isset($useStatements[strtolower($firstName)]))
                {
                    $phpDocParamType = $useStatements[strtolower($firstName)] 
                            . substr($phpDocParamType, strlen($firstName));
                }
                else
                {
                    $phpDocParamType = $m->getNamespaceName() . "\\" . $phpDocParamType;
                }
            }
            
        }
        
        
        if ($phpDocParamType !== null && $hintedParamType === null)
            return $phpDocParamType;
        else if ($phpDocParamType === null && $hintedParamType !== null)
            return $hintedParamType;
        else
        {
            //if ($hintedParamType !== $phpDocParamType)
            //    throw new \Exception("DocComment type differs from hinted type ($hintedParamType and $phpDocParamType)!");

            return $hintedParamType;
        }
    }  
}

class ReflectionFinder implements ClassFinderInterface
{
    public function findFile($class)
    {
        $c = new \ReflectionClass($class);
        return $c->getFileName();
    }
}