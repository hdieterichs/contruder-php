<?php

namespace Contruder\Php\TypeSystem\Reflector;

use \Contruder\Php\TypeSystem\PhpTypeSystem;

class ReflectionClass extends \ReflectionClass
{
    private $typeSystem;
    
    public function __construct($nameOrObject, PhpTypeSystem $typeSystem)
    {
        parent::__construct($nameOrObject);
        
        $this->typeSystem = $typeSystem;
    }
    
    /**
     * 
     * @return ReflectionMethod|null
     */
    public function getConstructor()
    {
        $result = parent::getConstructor();
        if ($result === null)
            return null;
        return new ReflectionMethod($result->class, 
                $result->name, $this->typeSystem);
    }
    
    /**
     * 
     * @param string $name
     * @return ReflectionMethod
     */
    public function getMethod($name)
    {
        $result = parent::getMethod($name);
        return new ReflectionMethod($result->class, 
                $result->name, $this->typeSystem);
    }
    
    public function getMethods($filter = null)
    {
        $methods = parent::getMethods($filter);
        $result = array();
        foreach ($methods as $m)
        {
            $result[] = new ReflectionMethod($m->class, 
                $m->name, $this->typeSystem);
        }
        
        return $result;
    }
}