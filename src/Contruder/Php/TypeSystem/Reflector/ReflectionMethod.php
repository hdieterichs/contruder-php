<?php

namespace Contruder\Php\TypeSystem\Reflector;

use Contruder\Php\TypeSystem\PhpTypeSystem;

class ReflectionMethod extends \ReflectionMethod
{
    private $typeSystem;
    
    public function __construct($class, $name, PhpTypeSystem $typeSystem)
    {
        parent::__construct($class, $name);
        
        $this->typeSystem = $typeSystem;
    }
    
    public function getParameters()
    {
        $result = array();
        $parameters = parent::getParameters();
        /* @var $p \ReflectionMethod */
        foreach ($parameters as $p)
        {
            $result[] = new ReflectionParameter(
                    array($this->getDeclaringClass()->getName(), 
                        $this->getName()), $p->getName(), $this->typeSystem);
        }
        
        return $result;
    }
}