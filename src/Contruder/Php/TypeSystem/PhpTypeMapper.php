<?php

namespace Contruder\Php\TypeSystem;

use Contruder\Common\StringHelper;
use Contruder\TypeSystem\TypeMapper;
use Contruder\TypeSystem\ContruderTypeId;
use Contruder\TypeSystem\UnknownTypeInfo;

class PhpTypeMapper extends TypeMapper
{
    /**
     * @var PhpTypeSystem
     */
    private $phpTypeSystem;

    public function __construct(PhpTypeSystem $phpTypeSystem)
    {
        $this->phpTypeSystem = $phpTypeSystem;
    }
    
    public function getType(ContruderTypeId $typeId, array $genericArguments = array())
    {
        $typeNs = $typeId->getNamespace();
        $typeName = $typeId->getName();

        //e.g. php-namespace:Contruder\TestClasses

        if (StringHelper::startsWith($typeNs, "php-namespace:"))
        {
            $phpNs = StringHelper::removeStart($typeNs, "php-namespace:");
            $phpNs = str_replace("/", "\\", $phpNs);

            return $this->phpTypeSystem->getType($phpNs . "\\" . $typeName);
        }
        
        if (StringHelper::startsWith($typeNs, "php-ns:"))
        {
            $phpNs = StringHelper::removeStart($typeNs, "php-ns:");
            $phpNs = str_replace("/", "\\", $phpNs);

            return $this->phpTypeSystem->getType($phpNs . "\\" . $typeName);
        }

        return UnknownTypeInfo::getUnknown($typeId->__toString());
    }

}