<?php

namespace Contruder\Php\TypeSystem;

class Php
{
    /**
     * 
     * @param string $fullTypeName
     * @return PhpType
     */
    public static function typeof($fullTypeName)
    {
        return PhpTypeSystem::getCurrent()->getType($fullTypeName);
    }

    public static function typeofBool()
    {
        return self::typeof("bool");
    }

    public static function typeofObject()
    {
        return self::typeof("object");
    }
    
    public static function typeofString()
    {
        return self::typeof("string");
    }
    
    public static function typeofInt()
    {
        return self::typeof("int");
    }
    
    public static function typeofDouble()
    {
        return self::typeof("double");
    }
}