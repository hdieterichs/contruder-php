<?php

namespace Contruder\Php\TypeSystem;

use Contruder\TypeSystem\PropertyFeature;
use Contruder\Common\Expect;
use Contruder\TypeSystem\ContruderType;

class PhpPropertyFeature extends PropertyFeature
{
    private $name;
    private $ownerType;
    private $requiredType;
    
    public function __construct($name, 
            ContruderType $ownerType, ContruderType $requiredType)
    {
        Expect::that($name)->isString();
        
        $this->name = $name;
        $this->ownerType = $ownerType;
        $this->requiredType = $requiredType;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function getOwnerType()
    {
        return $this->ownerType;
    }

    public function getRequiredType()
    {
        return $this->requiredType;
    }
}