<?php

namespace Contruder\Php\TypeSystem;

use Contruder\TypeSystem\AttachedPropertyFeature;
use Contruder\Common\Expect;
use Contruder\TypeSystem\ContruderType;

class PhpAttachedPropertyFeature extends AttachedPropertyFeature
{
    private $name;
    private $ownerType;
    private $requiredType;
    private $requiredTargetType;
    
    public function __construct($name, 
            ContruderType $ownerType, ContruderType $requiredType,
            ContruderType $requiredTargetType)
    {
        Expect::that($name)->isString();
        
        $this->name = $name;
        $this->ownerType = $ownerType;
        $this->requiredType = $requiredType;
        $this->requiredTargetType = $requiredTargetType;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function getOwnerType()
    {
        return $this->ownerType;
    }

    public function getRequiredType()
    {
        return $this->requiredType;
    }

    public function getRequiredTargetType()
    {
        return $this->requiredTargetType;
    }
}