<?php

namespace Contruder\TypeSystem;

abstract class AttachedPropertyFeature extends Feature
{
    /**
     * @return ContruderType
     */
    public abstract function getRequiredTargetType();
    
    /**
     * @return ContruderType
     */
    public abstract function getRequiredType();
}
