<?php

namespace Contruder\TypeSystem;

abstract class ConstructionSet
{
    /**
     * @return PropertyFeature[]
     */
    abstract function getRequiredFeatures();
}