<?php

namespace Contruder\TypeSystem;

abstract class ContruderType
{
    public abstract function getName();
    
    public abstract function getPredefinedValues();
    
    public abstract function getSupportedFeatures();
    
    public abstract function getDefaultAttachedProperty();
    
    public abstract function getDefaultProperties();
    
    public abstract function getConstructionSets();
    
    public abstract function getGenericTypeArguments();
    
    public abstract function isGenericTypeParameter();
    
    public abstract function getGenericTypeParameterOwner();
    
    public abstract function isGenericType();
     
    public abstract function getUnderlyingGenericType();
    
    public abstract function acceptsList();
    
    public abstract function getDerivedTypes();
    
    public abstract function inheritsFrom(ContruderType $type);
    
    public abstract function __toString();
}
