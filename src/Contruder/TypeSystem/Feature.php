<?php

namespace Contruder\TypeSystem;

abstract class Feature
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @return string
     */
    public abstract function getName();

    /**
     * @return ContruderType
     */
    public abstract function getOwnerType();
}