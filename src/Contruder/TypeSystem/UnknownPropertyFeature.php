<?php

namespace Contruder\TypeSystem;

use Contruder\Common\Expect;

class UnknownPropertyFeature extends PropertyFeature
{
    /**
     * @var ContruderType
     */
    private $ownerType;
    
    /**
     * @var string
     */
    private $name;
    
    public function __construct(ContruderType $ownerType, $name = "")
    {
        Expect::that($name)->isString();
        
        $this->ownerType = $ownerType;
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOwnerType()
    {
        return $this->ownerType;
    }

    public function getRequiredType()
    {
        return UnknownTypeInfo::getUnknown($this->name . "Type");
    }
}