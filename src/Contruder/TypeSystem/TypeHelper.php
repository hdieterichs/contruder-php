<?php

namespace Contruder\TypeSystem;

use Contruder\Common\Expect;

class TypeHelper
{
    public static function getDefaultAttachedPropertyFeatureOrUnknown(ContruderType $type)
    {
        $defaultAttachedProperty = $type->getDefaultAttachedProperty();
        if ($defaultAttachedProperty != null)
            return $defaultAttachedProperty;
        else
            return new UnknownAttachedPropertyFeature($type);
    }
    
    public static function getAttachedPropertyFeature(ContruderType $type,
            $attachedPropertyName)
    {
        Expect::that($attachedPropertyName)->isString();
        
        foreach ($type->getSupportedFeatures() as $feature)
        {
            if ($feature instanceof AttachedPropertyFeature)
            {
                if ($feature->getName() === $attachedPropertyName)
                    return $feature;
            }
        }
        
        return null;
    }
    
    public static function getAttachedPropertyFeatureOrUnknown(ContruderType $type,
            $attachedPropertyName)
    {
        Expect::that($attachedPropertyName)->isString();
        
        $result = self::getAttachedPropertyFeature($type, $attachedPropertyName);
        if ($result !== null)
            return $result;
        else
            return new UnknownAttachedPropertyFeature($type, $attachedPropertyName);
    }
    
    
    
    public static function getDefaultPropertyFeatureOrUnknown(ContruderType $type)
    {
        $defaultProperty = $type->getDefaultProperty();
        if ($defaultProperty !== null)
            return $defaultProperty;
        else
            return new UnknownPropertyFeature($type);
    }
    
    public static function getPropertyFeature(ContruderType $type, $propertyName)
    {
        Expect::that($propertyName)->isString();
        
        foreach ($type->getSupportedFeatures() as $feature)
        {
            if ($feature instanceof PropertyFeature)
            {
                if ($feature->getName() === $propertyName)
                    return $feature;
            }
        }
        
        return null;
    }
        
    public static function getPropertyFeatureOrUnknown(ContruderType $type, $propertyName)
    {
        Expect::that($propertyName)->isString();
        
        $result = self::getPropertyFeature($type, $propertyName);
        if ($result !== null)
            return $result;
        else
            return new UnknownPropertyFeature($type, $propertyName);
    }
}