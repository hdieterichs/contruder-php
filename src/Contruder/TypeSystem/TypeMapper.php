<?php

namespace Contruder\TypeSystem;

abstract class TypeMapper
{
    /**
     * @param ContruderTypeId $typeId
     * @param array $genericArguments
     * @return ContruderType
     */
    abstract function getType(ContruderTypeId $typeId, array $genericArguments = array());
}

