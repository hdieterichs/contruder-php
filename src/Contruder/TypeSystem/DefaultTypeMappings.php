<?php

namespace Contruder\TypeSystem;

class DefaultTypeMappings
{
    const ContruderXmlNamespace = "http://www.hediet.de/xsd/contruder/1.0/xml";
    const ContruderTypesNamespace = "http://www.hediet.de/xsd/contruder/1.0/types";

    public static function getObjectTypeId() { return self::getTypeId("Object"); }
    public static function getStringTypeId() { return self::getTypeId("String"); }
    public static function getIntTypeId() { return self::getTypeId("Int"); }
    public static function getDoubleTypeId() { return self::getTypeId("Double"); }
    public static function getBooleanTypeId() { return self::getTypeId("Boolean"); }
    public static function getArrayTypeId() { return self::getTypeId("Array"); }
    
    public static function getImplementationFactoryTypeId() { return self::getTypeId("SolutionFactory"); }
    public static function getTagTypeId() { return self::getTypeId("Tag"); }
    public static function getIdTypeId() { return self::getTypeId("Id"); }
    public static function getFirstTagElementTypeId() { return self::getTypeId("FirstTagElement"); }
    public static function getNextTagElementTypeId() { return self::getTypeId("NextTagElement"); }
    
    public static function getDirectoryInfoTypeId() { return self::getTypeId("DirectoryInfo"); }
    public static function getFileInfoTypeId() { return self::getTypeId("FileInfo"); }
    
    public static function getImplementationParserTypeId() { return self::getTypeId("ImplementationParser"); }
    public static function getValueProviderTypeId() { return self::getTypeId("IValueProvider"); }
    
    private static $cachedTypeIds = array();
    
    private static function getTypeId($name)
    {
        if (!isset(self::$cachedTypeIds[$name]))
            self::$cachedTypeIds[$name] = new ContruderTypeId(self::ContruderTypesNamespace, $name);
        return self::$cachedTypeIds[$name];
    }
}
