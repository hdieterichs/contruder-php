<?php

namespace Contruder\TypeSystem;

use Contruder\Common\Expect;

class ContruderTypeId
{
    private $name;
    private $namespace;
    
    public function __construct($namespace, $name)
    {
        Expect::that($namespace)->isString();
        Expect::that($name)->isString();
        
        $this->namespace = $namespace;
        $this->name = $name;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getNamespace()
    {
        return $this->namespace;
    }

    public function __toString()
    {
        return $this->namespace . "#" . $this->name;
    }
    
    public function equals($other)
    {
        if (!($other instanceof ContruderTypeId))
            return false;
        return ($this->namespace === $other->namespace) 
            && ($this->name === $other->name);
    }
}