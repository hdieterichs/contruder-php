<?php

namespace Contruder\TypeSystem;

abstract class PropertyFeature extends Feature
{
    /**
     * @return ContruderType
     */
    public abstract function getRequiredType();
}
