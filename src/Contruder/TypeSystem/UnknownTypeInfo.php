<?php

namespace Contruder\TypeSystem;

use Contruder\Common\Expect;

class UnknownTypeInfo extends ContruderType
{
    public static function getUnknown($name)
    {
        Expect::that($name)->isString();
        
        return new UnknownTypeInfo($name);
    }

    /**
     * @var string
     */
    private $name;
    
    private function __construct($name)
    {
        $this->name = $name;
    }
    
    public function acceptsList()
    {
        return true;
    }

    public function getConstructionSets()
    {
        return array();
    }

    public function getDefaultAttachedProperty()
    {
        return null;
    }

    public function getDefaultProperties()
    {
        return array();
    }

    public function getDerivedTypes()
    {
        throw new NotImplementedException();
    }

    public function getGenericTypeArguments()
    {
        throw new NotImplementedException();
    }

    public function getGenericTypeParameterOwner()
    {
        throw new NotImplementedException();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPredefinedValues()
    {
        return array();
    }

    public function getSupportedFeatures()
    {
        return array();
    }

    public function getUnderlyingGenericType()
    {
        throw new NotImplementedException();
    }

    public function inheritsFrom(ContruderType $type)
    {
        return false;
    }

    public function isGenericType()
    {
        return false;
    }

    public function isGenericTypeParameter()
    {
        return false;
    }

    public function __toString()
    {
        return "Unknown Type: " . $this->name;
    }

}