<?php

namespace Contruder\Parser;

use Contruder\Common\FlatServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\Parser\Services\CurrentDirectoryService;
use Contruder\Parser\Services\CurrentParserService;
use Contruder\Parser\Tyml\TymlParser;
use Contruder\Php\TypeSystem\PhpTypeMapper;
use Contruder\Php\TypeSystem\PhpTypeSystem;
use Contruder\TypeSystem\TypeMapper;
use Exception;
use Nunzion\IO\Directory;
use Nunzion\IO\File;

class DefaultParser
{
    /**
     * @param TypeMapper $typeMapper
     * @return ImplementationParser
     */
    public static function getParser(TypeMapper $typeMapper = null)
    {
        if ($typeMapper === null)
            $typeMapper = new PhpTypeMapper(PhpTypeSystem::getCurrent());
        
        return new _DefaultImplementationParser($typeMapper);
    }
}

class _DefaultImplementationParser extends ImplementationParser
{
    /**
     * @var TypeMapper
     */
    private $typeMapper;

    public function __construct(TypeMapper $typeMapper)
    {
        $this->typeMapper = $typeMapper;
    }

    /**
     * @param string $content
     * @param string $filename
     * @return Implementation
     */
    public function parse($content, $filename = null)
    {
        if ($this->typeMapper === null)
            $this->typeMapper = new PhpTypeMapper(PhpTypeSystem::getCurrent());
        
        $tymlParser = new TymlParser($this->typeMapper);

        $services = array();
        $services[CurrentParserService::getClassName()] = new _CurrentParserService($this);
        $services[CurrentDirectoryService::getClassName()] = new _CurrentDirectoryService($filename);
        
        $result = $tymlParser->parse($content, new FlatServiceProvider($services));
        
        if (!$result->isSuccessful())
            throw new ParseException($result);

        return $result->getImplementation();
    }
}

class _CurrentDirectoryService extends CurrentDirectoryService
{
    /**
     * @var Directory
     */
    private $currentDirectory;

    public function __construct($filename)
    {
        if ($filename != null)
            $this->currentDirectory = (new File($filename))->getDirectory();
    }
    
    public function getCurrentDirectory()
    {
        if ($this->currentDirectory === null)
            throw new Exception("No current directory available.");
        return $this->currentDirectory;
    }
    
    public function hasCurrentDirectory()
    {
        return $this->currentDirectory !== null;
    }
}

class _CurrentParserService extends CurrentParserService
{
    /**
     * @var ImplementationParser
     */
    private $currentParser;

    public function __construct(ImplementationParser $currentParser)
    {
        $this->currentParser = $currentParser;
    }

    /**
     * @return ImplementationParser
     */
    public function getCurrentParser()
    {
        return $this->currentParser;
    }
}