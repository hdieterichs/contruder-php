<?php

namespace Contruder\Parser\Services;

use Nunzion\IO\Directory;

abstract class CurrentDirectoryService
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @return Directory
     */
    public abstract function getCurrentDirectory();
    
    /**
     * @return boolean
     */
    public abstract function hasCurrentDirectory();
} 