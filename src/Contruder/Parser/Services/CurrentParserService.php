<?php


namespace Contruder\Parser\Services;

use Contruder\Parser\ImplementationParser;

abstract class CurrentParserService
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @return ImplementationParser
     */
    public abstract function getCurrentParser();
} 