<?php

namespace Contruder\Parser\Tyml;

use Contruder\Common\Expect;
use Contruder\ImplementationTree\Implementation;
use Tyml\Parser\Message;

class ParseResult
{

    /**
     * @var Implementation
     */
    private $implementation;

    /**
     * @var Message[]
     */
    private $messages;

    public function __construct(Implementation $implementation, array $messages)
    {
        Expect::that($messages)->isArrayOf(Message::getClassName());

        $this->implementation = $implementation;
        $this->messages = $messages;
    }

    /**
     * @return Message[]
     */
    public function getParseMessages()
    {
        return $this->messages;
    }
    
    public function isSuccessful()
    {
        return count($this->messages) === 0;
    }
    
    /**
     * 
     * @return Implementation
     */
    public function getImplementation()
    {
        return $this->implementation;
    }

}
