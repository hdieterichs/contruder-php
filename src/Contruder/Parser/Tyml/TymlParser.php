<?php

namespace Contruder\Parser\Tyml;

use Contruder\Common\ServiceProvider;
use Contruder\ImplementationTree\Implementation;
use Contruder\TypeSystem\TypeMapper;
use Tyml\Parser\Parser;

class TymlParser
{
    /**
     * @var TypeMapper
     */
    private $typeMapper;

    public function __construct(TypeMapper $typeMapper)
    {
        $this->typeMapper = $typeMapper;
    }

    /**
     *
     * @param string $content
     * @param ServiceProvider $serviceProvider
     * @internal param string $filename
     * @internal param string $type
     * @return ParseResult
     */
    public function parse($content, ServiceProvider $serviceProvider)
    {
        $p = new Parser();
        $result = $p->parse($content);
        
        $transformer = new TymlTransformer($this->typeMapper);
        $implementation = $transformer->accept($result->getDocument(),
            new TransformationState(null, null, $serviceProvider));

        return new ParseResult($implementation, $result->getMessages());
    }
}
