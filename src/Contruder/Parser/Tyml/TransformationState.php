<?php

namespace Contruder\Parser\Tyml;

use Contruder\Common\ServiceProvider;
use Contruder\Parser\Services\CurrentParserService;
use Contruder\TypeSystem\ContruderType;
use Contruder\Parser\Services\CurrentDirectoryService;
use Contruder\ImplementationTree\Implementation;
use Exception;

class TransformationState 
{
    private $expectedType;
    /**
     * @var TransformationState
     */
    private $parent;
    /**
     * @var ServiceProvider
     */
    private $serviceProvider;

    private $currentServiceTag;
    private $currentServiceId;
    
    public function __construct($expectedType = null, 
            TransformationState $parent = null, 
            ServiceProvider $serviceProvider = null,
            Implementation $currentServiceTag = null, 
            Implementation $currentServiceId = null)
    {
        $this->expectedType = $expectedType;
        $this->parent = $parent;
        $this->serviceProvider = $serviceProvider;
        $this->currentServiceTag = $currentServiceTag;
        $this->currentServiceId = $currentServiceId;
    }

    /**
     * @return CurrentParserService
     */
    public function getCurrentParserService()
    {
        if ($this->serviceProvider === null)
        {
            if ($this->parent === null)
                throw new Exception("Current parser service is not set");
            return $this->parent->getCurrentParserService();
        }
        
        return $this->serviceProvider->requireServiceOfType(CurrentParserService::getClassName());
    }

    /**
     * @return CurrentDirectoryService
     */
    public function getCurrentDirectoryService()
    {
        if ($this->serviceProvider === null)
        {
            if ($this->parent === null)
                throw new Exception("Current directory service is not set");
            return $this->parent->getCurrentDirectoryService();
        }
        
        return $this->serviceProvider->requireServiceOfType(CurrentDirectoryService::getClassName());
    }
    
    /**
     * @return Implementation
     */
    public function getCurrentServiceTag()
    {
        if ($this->currentServiceTag === null)
        {
            if ($this->parent === null)
                return null;
            return $this->parent->getCurrentServiceTag();
        }
        return $this->currentServiceTag;
    }
    
    /**
     * @return Implementation
     */
    public function getCurrentServiceId()
    {
        if ($this->currentServiceId === null)
        {
            if ($this->parent === null)
                return null;
            return $this->parent->getCurrentServiceId();
        }
        return $this->currentServiceId;
    }
    
    /**
     * @return ContruderType
     */
    public function getExpectedType()
    {
        return $this->expectedType;
    }
} 