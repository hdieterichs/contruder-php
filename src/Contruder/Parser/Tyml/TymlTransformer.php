<?php

namespace Contruder\Parser\Tyml;

use ArrayIterator;
use Contruder\Common\Expect;
use Contruder\ImplementationTree\DirectImplementation;
use Contruder\ImplementationTree\Implementation;
use Contruder\ImplementationTree\ImplementationHelper;
use Contruder\ImplementationTree\ListImplementation;
use Contruder\ImplementationTree\PropertyFeatureRealization;
use Contruder\ImplementationTree\TypeImplementation;
use Contruder\Php\Runtime\CurrentParser;
use Contruder\Php\Runtime\Services\DynamicService;
use Contruder\Php\Runtime\Services\Id;
use Contruder\Php\Runtime\Services\Next;
use Contruder\Php\Runtime\Services\Tag;
use Contruder\Php\TypeSystem\Php;
use Contruder\Php\TypeSystem\PhpTypeSystem;
use Contruder\TypeSystem\ContruderTypeId;
use Contruder\TypeSystem\DefaultTypeMappings;
use Contruder\TypeSystem\PropertyFeature;
use Contruder\TypeSystem\TypeHelper;
use Contruder\TypeSystem\TypeMapper;
use Nunzion\IO\Directory;
use Nunzion\IO\File;
use Nunzion\Types\Type;
use Tyml\Ast\AstVisitor;
use Tyml\Ast\TymlArray;
use Tyml\Ast\TymlDocument;
use Tyml\Ast\TymlNode;
use Tyml\Ast\TymlObject;
use Tyml\Ast\TymlPrimitive;
use Tyml\Ast\TymlString;

class TymlTransformer extends AstVisitor
{
    /**
     * @var TypeMapper
     */
    private $typeMapper;
    private $stringType;
    private $arrayType;
    private $parserType;

    public function __construct(TypeMapper $typeMapper)
    {
        $this->typeMapper = $typeMapper;
        $this->stringType = $this->typeMapper->getType(DefaultTypeMappings::getStringTypeId());
        $this->arrayType = $this->typeMapper->getType(DefaultTypeMappings::getArrayTypeId());
        $this->parserType = $this->typeMapper->getType(DefaultTypeMappings::getImplementationParserTypeId());
    }

    /**
     * @param TymlNode $node
     * @param TransformationState $state
     * @return Implementation
     */
    public function accept(TymlNode $node, $state = null)
    {
        Expect::that($state)->isNotNull();
        
        return parent::accept($node, $state);
    }


    /**
     * @param TymlDocument $node
     * @param TransformationState $state
     * @return Implementation
     */
    protected function visitDocument(TymlDocument $node, $state)
    {
        return $this->accept($node->getRootNode(), $state);
    }

    /**
     * @param TymlDocument $node
     * @param TransformationState $state
     * @return Implementation
     */
    protected function visitObject(TymlObject $node, $state)
    {
        $typeNamespace = $node->getTypeIdentifier()->getNamespace();
        $typeName = $node->getTypeIdentifier()->getName();

        $type = $this->typeMapper->getType(new ContruderTypeId($typeNamespace, $typeName));
        
        if ($type->getName() === CurrentParser::getClassName())
        {
            //TODO move dependencies to classes to the type mapper.
            $r = $state->getCurrentParserService()->getCurrentParser();
            $t = $state->getExpectedType();
            return new DirectImplementation($t, $r);
        }
        
        $featureRealizations = array();

        $childState = $state;
        $isImplService = false;
        if ($type->getName() === DynamicService::getClassName())
        {
            $isImplService = true;
        }
        
        foreach ($node->getAttributes() as $attribute)
        {
            $attributeName = $attribute->getIdentifier()->getName();
            $feature = TypeHelper::getPropertyFeatureOrUnknown($type, $attributeName);

            $implementation = $this->accept($attribute->getValue(), 
                    new TransformationState($feature->getRequiredType(), $childState));

            if ($isImplService && $feature->getName() === "Tag")
                $childState = new TransformationState(null, 
                        $childState, null, $implementation, null);
            if ($isImplService && $feature->getName() === "Id")
                $childState = new TransformationState(null, 
                        $childState, null, null, $implementation);
            
            $featureRealizations[$feature->getName()] = new PropertyFeatureRealization($feature, $implementation);
        }
        
        $defaultProperties = $type->getDefaultProperties();
        $iterator = new ArrayIterator($defaultProperties);
        
        foreach ($node->getImplicitAttributes() as $value)
        {
            if (!$iterator->valid())
                break;
            /* @var $feature PropertyFeature */
            $feature = $iterator->current();
            
            $implementation = $this->accept($value, new TransformationState($feature->getRequiredType(), $childState));
            
            if ($isImplService && $feature->getName() === "Tag")
                $childState = new TransformationState(null, 
                        $childState, null, $implementation, null);
            else if ($isImplService && $feature->getName() === "Id")
                $childState = new TransformationState(null, 
                        $childState, null, null, $implementation);
            
            $featureRealizations[$feature->getName()] = new PropertyFeatureRealization($feature, $implementation);
            
            $iterator->next();
        }
        
        if ($type->getName() === Next::getClassName())
        {
            if (!isset($featureRealizations["Tag"]) && !isset($featureRealizations["Id"]))
            {
                if ($state->getCurrentServiceId() === null)
                    throw new \Exception("Service id not set for " . $node->getTextRegion());
                if ($state->getCurrentServiceTag() === null)
                {
                    throw new \Exception("Service tag not set for " . $node->getTextRegion());
                }
                
                $feature = TypeHelper::getPropertyFeature($type, "Id");
                $featureRealizations["Id"] = 
                        new PropertyFeatureRealization($feature, $state->getCurrentServiceId());

                $feature = TypeHelper::getPropertyFeature($type, "Tag");
                $featureRealizations["Tag"] = 
                        new PropertyFeatureRealization($feature, $state->getCurrentServiceTag());
            }
        }
        
        return new TypeImplementation($type, $featureRealizations);
    }

    /**
     * @param TymlDocument $node
     * @param TransformationState $state
     * @return Implementation
     */
    protected function visitPrimitive(TymlPrimitive $node, $state)
    {
        $t = null;
        $v = $node->getValue();
        if ($v === "true")
        {
            $r = true;
            $t = Php::typeofBool();
        }
        else if ($v === "false")
        {
            $r = false;
            $t = Php::typeofBool();
        }
        else if ($v === "null")
        {
            $r = null;
            $t = Php::typeofObject();
        }
        else
        {
            $r = (int)$node->getValue();
            $t = Php::typeofInt();
        }

        return new DirectImplementation($t, $r);
    }

    /**
     * 
     * @param TymlArray $node
     * @param TransformationState $state
     * @return ListImplementation
     */
    protected function visitArray(TymlArray $node, $state)
    {
        $items = array();

        $subState = new TransformationState($state->getExpectedType()->getGenericTypeArguments()[0], $state);
        
        foreach ($node->getItems() as $item)
        {
            $items[] = $this->accept($item, $subState);
        }

        return new ListImplementation($this->arrayType, $items);
    }

    private function resolveIdentifier($identifier, TymlNode $node)
    {
        $items = explode("/", $identifier, 2);
        $ns = "";
        $name = "";
        if (count($items) < 2)
        {
            $ns = $node->resolvePrefix("");
            $name = $items[0];
        }
        else
        {
            $ns = $node->resolvePrefix($items[0]);
            $name = $items[1];
        }
        
        return (object)array("ns" => $ns, "name" => $name);
    }
    
    /**
     * 
     * @param TymlString $node
     * @param TransformationState $state
     * @return DirectImplementation
     */
    protected function visitString(TymlString $node, $state)
    {
        $stringType = $this->typeMapper->getType(DefaultTypeMappings::getStringTypeId());
        
        if ($state->getExpectedType()->getName() === "Nunzion\IO\Directory")
        {
            $dir = $node->getValue();

            $currentDirectory = $state->getCurrentDirectoryService()->getCurrentDirectory();

            return ImplementationHelper::newTypeImplementation($state->getExpectedType(), array("Path" =>
                new DirectImplementation($stringType, Directory::makeAbsolute($dir, $currentDirectory)->getPath())));
        }
        if ($state->getExpectedType()->getName() === "Nunzion\IO\File")
        {
            $file = $node->getValue();

            $currentDirectory = $state->getCurrentDirectoryService()->getCurrentDirectory();

            return ImplementationHelper::newTypeImplementation($state->getExpectedType(), array("Path" =>
                new DirectImplementation($stringType, File::makeAbsolute($file, $currentDirectory)->getPath())));
        }
        if ($state->getExpectedType()->getName() === Type::_getClassName())
        {
            $r = $this->resolveIdentifier($node->getValue(), $node);
            
            $contruderType = $this->typeMapper->getType(new ContruderTypeId($r->ns, $r->name));

            return ImplementationHelper::newTypeImplementation(
                    PhpTypeSystem::getCurrent()->getType("Contruder\Php\Runtime\TypeRef"), 
                    array("TypeName" => new DirectImplementation($stringType, $contruderType->getName())));
        }
        if ($state->getExpectedType()->getName() === "Hediet\Types\ObjectType")
        {
            $r = $this->resolveIdentifier($node->getValue(), $node);
            
            $contruderType = $this->typeMapper->getType(new ContruderTypeId($r->ns, $r->name));
            $type = \Hediet\Types\Type::of($contruderType->getName());

            return new DirectImplementation($state->getExpectedType(), $type);
        }
        
        
        //TODO
        else if ($state->getExpectedType()->getName() === "Contruder\Php\Runtime\Services\Tag")
        {    
            $r = $this->resolveIdentifier($node->getValue(), $node);
               
            return ImplementationHelper::newTypeImplementation($state->getExpectedType(), 
                    array("NamespaceUri" => new DirectImplementation($stringType, $r->ns), 
                        "Name" => new DirectImplementation($stringType, $r->name)));
        }
        else if ($state->getExpectedType()->getName() === "Contruder\Php\Runtime\Services\Id")
        {    
            $r = $this->resolveIdentifier($node->getValue(), $node);
            
            return ImplementationHelper::newTypeImplementation($state->getExpectedType(), 
                    array("NamespaceUri" => new DirectImplementation($stringType, $r->ns), 
                        "Name" => new DirectImplementation($stringType, $r->name)));
        }

        return new DirectImplementation($this->stringType, $node->getValue());
    }
}