<?php

namespace Contruder\Parser;

use Contruder\Parser\Tyml\ParseResult;
use Exception;

class ParseException extends Exception
{
    /**
     *
     * @var ParseResult
     */
    private $parseResult;
    
    public function __construct(ParseResult $parseResult)
    {
        $this->parseResult = $parseResult;
    }
    
    /**
     * 
     * @return ParseResult
     */
    public function getParseResult()
    {
        return $this->parseResult;
    }
}
