<?php

namespace Contruder\Parser;

use Contruder\ImplementationTree\Implementation;

abstract class ImplementationParser
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @param string $content
     * @param string $filename
     * @return Implementation
     */
    public abstract function parse($content, $filename = null);

    /**
     * @param string $filename
     * @return Implementation
     */
    public function parseFile($filename)
    {
        $content = file_get_contents($filename);
        $result = $this->parse($content, $filename);
        return $result;
    }
}